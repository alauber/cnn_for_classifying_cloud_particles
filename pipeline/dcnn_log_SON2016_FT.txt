args: Namespace(augment=True, batch_size=256, batchnorm=True, dropout=1, early_stopping_epochs=200, img_size=32, l2_reg=0, learning_rate=0.0001, log_dir='dcnn_pipeline', max_epochs=5000, model_dir='models/dcnn_model_2016_SON', pred_dir='predictions', pred_set=None, predict=False, residual=True, resume_training=True, seed=None, suffix='_SON2016_FT', test=False, test_set=None, train=True, train_set='dcnn_pipeline/train_pipeline.pkl', val=True, val_set='dcnn_pipeline/val_pipeline.pkl')

epoch: 1, step: 50
training loss: 0.48793, validation loss: 0.57304
accuracy: 0.94665, macro average f1 score: 0.94196
classification report:
             precision    recall  f1-score   support

          0    0.96967   0.92006   0.94422      1251
          1    0.93377   0.97578   0.95431      1445
          2    0.92841   0.92627   0.92734       434

avg / total    0.94738   0.94665   0.94654      3130

epochs without improvement: 0, model saved
----------------------------------------------------
epoch: 2, step: 99
training loss: 0.31444, validation loss: 0.46841
accuracy: 0.95559, macro average f1 score: 0.95185
classification report:
             precision    recall  f1-score   support

          0    0.97727   0.92806   0.95203      1251
          1    0.94474   0.98201   0.96301      1445
          2    0.93409   0.94700   0.94050       434

avg / total    0.95627   0.95559   0.95550      3130

epochs without improvement: 0, model saved
----------------------------------------------------
epoch: 3, step: 148
training loss: 0.28071, validation loss: 0.39327
accuracy: 0.95911, macro average f1 score: 0.95757
classification report:
             precision    recall  f1-score   support

          0    0.97432   0.94005   0.95688      1251
          1    0.95013   0.97578   0.96279      1445
          2    0.94761   0.95853   0.95304       434

avg / total    0.95945   0.95911   0.95907      3130

epochs without improvement: 0, model saved
----------------------------------------------------
epoch: 4, step: 197
training loss: 0.27113, validation loss: 0.38091
accuracy: 0.96134, macro average f1 score: 0.95984
classification report:
             precision    recall  f1-score   support

          0    0.97765   0.94404   0.96055      1251
          1    0.95024   0.97785   0.96385      1445
          2    0.95402   0.95622   0.95512       434

avg / total    0.96172   0.96134   0.96132      3130

epochs without improvement: 0, model saved
----------------------------------------------------
epoch: 5, step: 246
training loss: 0.25057, validation loss: 0.34740
accuracy: 0.96166, macro average f1 score: 0.96004
classification report:
             precision    recall  f1-score   support

          0    0.97925   0.94325   0.96091      1251
          1    0.95519   0.97370   0.96436      1445
          2    0.93584   0.97465   0.95485       434

avg / total    0.96213   0.96166   0.96166      3130

epochs without improvement: 0, model saved
----------------------------------------------------
epoch: 6, step: 295
training loss: 0.23763, validation loss: 0.33081
accuracy: 0.96134, macro average f1 score: 0.96222
classification report:
             precision    recall  f1-score   support

          0    0.96437   0.95204   0.95817      1251
          1    0.96008   0.96540   0.96273      1445
          2    0.95701   0.97465   0.96575       434

avg / total    0.96137   0.96134   0.96133      3130

epochs without improvement: 0, model saved
----------------------------------------------------
epoch: 7, step: 344
training loss: 0.22509, validation loss: 0.30973
accuracy: 0.96741, macro average f1 score: 0.96815
classification report:
             precision    recall  f1-score   support

          0    0.98177   0.94724   0.96420      1251
          1    0.95559   0.98270   0.96895      1445
          2    0.96796   0.97465   0.97130       434

avg / total    0.96777   0.96741   0.96738      3130

epochs without improvement: 0, model saved
----------------------------------------------------
epoch: 8, step: 393
training loss: 0.22036, validation loss: 0.29752
accuracy: 0.96869, macro average f1 score: 0.96895
classification report:
             precision    recall  f1-score   support

          0    0.98346   0.95044   0.96667      1251
          1    0.95814   0.98201   0.96992      1445
          2    0.96364   0.97696   0.97025       434

avg / total    0.96902   0.96869   0.96867      3130

epochs without improvement: 0, model saved
----------------------------------------------------
epoch: 9, step: 442
training loss: 0.20931, validation loss: 0.29899
accuracy: 0.96422, macro average f1 score: 0.96388
classification report:
             precision    recall  f1-score   support

          0    0.96702   0.96083   0.96391      1251
          1    0.96862   0.96125   0.96492      1445
          2    0.94260   0.98387   0.96280       434

avg / total    0.96437   0.96422   0.96422      3130

epochs without improvement: 1
----------------------------------------------------
epoch: 10, step: 491
training loss: 0.20002, validation loss: 0.28740
accuracy: 0.96869, macro average f1 score: 0.96946
classification report:
             precision    recall  f1-score   support

          0    0.97016   0.96163   0.96588      1251
          1    0.96828   0.97163   0.96995      1445
          2    0.96591   0.97926   0.97254       434

avg / total    0.96870   0.96869   0.96868      3130

epochs without improvement: 0, model saved
----------------------------------------------------
epoch: 11, step: 540
training loss: 0.19320, validation loss: 0.26949
accuracy: 0.96997, macro average f1 score: 0.97020
classification report:
             precision    recall  f1-score   support

          0    0.98109   0.95364   0.96717      1251
          1    0.96392   0.97993   0.97186      1445
          2    0.95955   0.98387   0.97156       434

avg / total    0.97018   0.96997   0.96994      3130

epochs without improvement: 0, model saved
----------------------------------------------------
epoch: 12, step: 589
training loss: 0.18288, validation loss: 0.26777
accuracy: 0.96997, macro average f1 score: 0.97096
classification report:
             precision    recall  f1-score   support

          0    0.97792   0.95604   0.96686      1251
          1    0.96451   0.97785   0.97113      1445
          2    0.96606   0.98387   0.97489       434

avg / total    0.97008   0.96997   0.96994      3130

epochs without improvement: 0, model saved
----------------------------------------------------
epoch: 13, step: 638
training loss: 0.18678, validation loss: 0.26896
accuracy: 0.96997, macro average f1 score: 0.97001
classification report:
             precision    recall  f1-score   support

          0    0.97182   0.96483   0.96831      1251
          1    0.97226   0.97024   0.97125      1445
          2    0.95740   0.98387   0.97045       434

avg / total    0.97002   0.96997   0.96997      3130

epochs without improvement: 1
----------------------------------------------------
epoch: 14, step: 687
training loss: 0.18675, validation loss: 0.25845
accuracy: 0.97157, macro average f1 score: 0.97194
classification report:
             precision    recall  f1-score   support

          0    0.98116   0.95763   0.96926      1251
          1    0.96463   0.98131   0.97290      1445
          2    0.96811   0.97926   0.97365       434

avg / total    0.97172   0.97157   0.97155      3130

epochs without improvement: 0, model saved
----------------------------------------------------
epoch: 15, step: 736
training loss: 0.17635, validation loss: 0.26599
accuracy: 0.96965, macro average f1 score: 0.97094
classification report:
             precision    recall  f1-score   support

          0    0.96865   0.96323   0.96593      1251
          1    0.97028   0.97163   0.97095      1445
          2    0.97039   0.98157   0.97595       434

avg / total    0.96964   0.96965   0.96964      3130

epochs without improvement: 1
----------------------------------------------------
epoch: 16, step: 785
training loss: 0.16725, validation loss: 0.25498
accuracy: 0.97284, macro average f1 score: 0.97319
classification report:
             precision    recall  f1-score   support

          0    0.98200   0.95923   0.97048      1251
          1    0.96662   0.98201   0.97425      1445
          2    0.96818   0.98157   0.97483       434

avg / total    0.97298   0.97284   0.97283      3130

epochs without improvement: 0, model saved
----------------------------------------------------
epoch: 17, step: 834
training loss: 0.16059, validation loss: 0.25527
accuracy: 0.97061, macro average f1 score: 0.97092
classification report:
             precision    recall  f1-score   support

          0    0.98031   0.95524   0.96761      1251
          1    0.96460   0.98062   0.97255      1445
          2    0.96380   0.98157   0.97260       434

avg / total    0.97077   0.97061   0.97058      3130

epochs without improvement: 1
----------------------------------------------------
epoch: 18, step: 883
training loss: 0.15568, validation loss: 0.25163
accuracy: 0.97125, macro average f1 score: 0.97170
classification report:
             precision    recall  f1-score   support

          0    0.98512   0.95284   0.96871      1251
          1    0.96209   0.98339   0.97262      1445
          2    0.96388   0.98387   0.97377       434

avg / total    0.97154   0.97125   0.97122      3130

epochs without improvement: 0, model saved
----------------------------------------------------
epoch: 19, step: 932
training loss: 0.15793, validation loss: 0.26344
accuracy: 0.97188, macro average f1 score: 0.97246
classification report:
             precision    recall  f1-score   support

          0    0.96887   0.97042   0.96965      1251
          1    0.97498   0.97093   0.97295      1445
          2    0.97032   0.97926   0.97477       434

avg / total    0.97189   0.97188   0.97188      3130

epochs without improvement: 1
----------------------------------------------------
epoch: 20, step: 981
training loss: 0.15572, validation loss: 0.24183
accuracy: 0.97252, macro average f1 score: 0.97267
classification report:
             precision    recall  f1-score   support

          0    0.97805   0.96163   0.96977      1251
          1    0.96984   0.97924   0.97452      1445
          2    0.96599   0.98157   0.97371       434

avg / total    0.97259   0.97252   0.97251      3130

epochs without improvement: 0, model saved
----------------------------------------------------
epoch: 21, step: 1030
training loss: 0.15171, validation loss: 0.26663
accuracy: 0.97061, macro average f1 score: 0.97119
classification report:
             precision    recall  f1-score   support

          0    0.97256   0.96323   0.96787      1251
          1    0.96970   0.97439   0.97204      1445
          2    0.96811   0.97926   0.97365       434

avg / total    0.97062   0.97061   0.97060      3130

epochs without improvement: 1
----------------------------------------------------
epoch: 22, step: 1079
training loss: 0.14267, validation loss: 0.27657
accuracy: 0.96997, macro average f1 score: 0.97015
classification report:
             precision    recall  f1-score   support

          0    0.96579   0.97042   0.96810      1251
          1    0.97226   0.97024   0.97125      1445
          2    0.97448   0.96774   0.97110       434

avg / total    0.96998   0.96997   0.96997      3130

epochs without improvement: 2
----------------------------------------------------
epoch: 23, step: 1128
training loss: 0.14441, validation loss: 0.26238
accuracy: 0.97380, macro average f1 score: 0.97339
classification report:
             precision    recall  f1-score   support

          0    0.97583   0.96803   0.97191      1251
          1    0.97316   0.97855   0.97585      1445
          2    0.97018   0.97465   0.97241       434

avg / total    0.97381   0.97380   0.97380      3130

epochs without improvement: 3
----------------------------------------------------
epoch: 24, step: 1177
training loss: 0.13703, validation loss: 0.26325
accuracy: 0.97093, macro average f1 score: 0.97023
classification report:
             precision    recall  f1-score   support

          0    0.97341   0.96563   0.96950      1251
          1    0.97367   0.97232   0.97299      1445
          2    0.95516   0.98157   0.96818       434

avg / total    0.97100   0.97093   0.97093      3130

epochs without improvement: 4
----------------------------------------------------
epoch: 25, step: 1226
training loss: 0.13238, validation loss: 0.27123
accuracy: 0.97188, macro average f1 score: 0.97113
classification report:
             precision    recall  f1-score   support

          0    0.97808   0.96323   0.97060      1251
          1    0.96787   0.97993   0.97387      1445
          2    0.96782   0.97005   0.96893       434

avg / total    0.97195   0.97188   0.97188      3130

epochs without improvement: 5
----------------------------------------------------
epoch: 26, step: 1275
training loss: 0.12974, validation loss: 0.27138
accuracy: 0.96933, macro average f1 score: 0.96999
classification report:
             precision    recall  f1-score   support

          0    0.96276   0.97122   0.96697      1251
          1    0.97686   0.96401   0.97039      1445
          2    0.96380   0.98157   0.97260       434

avg / total    0.96941   0.96933   0.96933      3130

epochs without improvement: 6
----------------------------------------------------
epoch: 27, step: 1324
training loss: 0.13146, validation loss: 0.29802
accuracy: 0.96326, macro average f1 score: 0.96454
classification report:
             precision    recall  f1-score   support

          0    0.94708   0.97282   0.95978      1251
          1    0.98069   0.94879   0.96447      1445
          2    0.95526   0.98387   0.96935       434

avg / total    0.96373   0.96326   0.96327      3130

epochs without improvement: 7
----------------------------------------------------
epoch: 28, step: 1373
training loss: 0.11999, validation loss: 0.28993
accuracy: 0.96645, macro average f1 score: 0.96729
classification report:
             precision    recall  f1-score   support

          0    0.95817   0.97042   0.96426      1251
          1    0.97671   0.95779   0.96716      1445
          2    0.95740   0.98387   0.97045       434

avg / total    0.96662   0.96645   0.96645      3130

epochs without improvement: 8
----------------------------------------------------
epoch: 29, step: 1422
training loss: 0.12198, validation loss: 0.25681
accuracy: 0.97508, macro average f1 score: 0.97512
classification report:
             precision    recall  f1-score   support

          0    0.98362   0.96003   0.97168      1251
          1    0.97003   0.98547   0.97769      1445
          2    0.96825   0.98387   0.97600       434

avg / total    0.97521   0.97508   0.97505      3130

epochs without improvement: 9
----------------------------------------------------
epoch: 30, step: 1471
training loss: 0.12275, validation loss: 0.26719
accuracy: 0.97125, macro average f1 score: 0.97142
classification report:
             precision    recall  f1-score   support

          0    0.96958   0.96803   0.96880      1251
          1    0.97367   0.97232   0.97299      1445
          2    0.96804   0.97696   0.97248       434

avg / total    0.97125   0.97125   0.97124      3130

epochs without improvement: 10
----------------------------------------------------
epoch: 31, step: 1520
training loss: 0.11146, validation loss: 0.27755
accuracy: 0.96613, macro average f1 score: 0.96647
classification report:
             precision    recall  f1-score   support

          0    0.96105   0.96643   0.96373      1251
          1    0.97273   0.96263   0.96765      1445
          2    0.95928   0.97696   0.96804       434

avg / total    0.96619   0.96613   0.96614      3130

epochs without improvement: 11
----------------------------------------------------
epoch: 32, step: 1569
training loss: 0.11727, validation loss: 0.28472
accuracy: 0.97188, macro average f1 score: 0.97107
classification report:
             precision    recall  f1-score   support

          0    0.97961   0.96003   0.96972      1251
          1    0.96601   0.98339   0.97462      1445
          2    0.96998   0.96774   0.96886       434

avg / total    0.97199   0.97188   0.97186      3130

epochs without improvement: 12
----------------------------------------------------
epoch: 33, step: 1618
training loss: 0.11008, validation loss: 0.26547
accuracy: 0.97284, macro average f1 score: 0.97292
classification report:
             precision    recall  f1-score   support

          0    0.97575   0.96483   0.97026      1251
          1    0.97245   0.97716   0.97480      1445
          2    0.96599   0.98157   0.97371       434

avg / total    0.97287   0.97284   0.97283      3130

epochs without improvement: 13
----------------------------------------------------
epoch: 34, step: 1667
training loss: 0.11373, validation loss: 0.28364
accuracy: 0.97316, macro average f1 score: 0.97258
classification report:
             precision    recall  f1-score   support

          0    0.98042   0.96083   0.97053      1251
          1    0.96801   0.98408   0.97598      1445
          2    0.97011   0.97235   0.97123       434

avg / total    0.97326   0.97316   0.97314      3130

epochs without improvement: 14
----------------------------------------------------
epoch: 35, step: 1716
training loss: 0.10831, validation loss: 0.29244
accuracy: 0.96613, macro average f1 score: 0.96747
classification report:
             precision    recall  f1-score   support

          0    0.95652   0.96723   0.96184      1251
          1    0.97607   0.95986   0.96790      1445
          2    0.96171   0.98387   0.97267       434

avg / total    0.96627   0.96613   0.96614      3130

epochs without improvement: 15
----------------------------------------------------
epoch: 36, step: 1765
training loss: 0.10386, validation loss: 0.28710
accuracy: 0.97093, macro average f1 score: 0.97139
classification report:
             precision    recall  f1-score   support

          0    0.96950   0.96563   0.96756      1251
          1    0.97236   0.97370   0.97303      1445
          2    0.97025   0.97696   0.97359       434

avg / total    0.97092   0.97093   0.97092      3130

epochs without improvement: 16
----------------------------------------------------
epoch: 37, step: 1814
training loss: 0.09965, validation loss: 0.27321
accuracy: 0.97252, macro average f1 score: 0.97243
classification report:
             precision    recall  f1-score   support

          0    0.97727   0.96243   0.96979      1251
          1    0.97180   0.97785   0.97482      1445
          2    0.96171   0.98387   0.97267       434

avg / total    0.97259   0.97252   0.97251      3130

epochs without improvement: 17
----------------------------------------------------
epoch: 38, step: 1863
training loss: 0.10009, validation loss: 0.29316
accuracy: 0.96933, macro average f1 score: 0.96917
classification report:
             precision    recall  f1-score   support

          0    0.97637   0.95763   0.96691      1251
          1    0.96580   0.97716   0.97145      1445
          2    0.96145   0.97696   0.96914       434

avg / total    0.96942   0.96933   0.96931      3130

epochs without improvement: 18
----------------------------------------------------
epoch: 39, step: 1912
training loss: 0.09607, validation loss: 0.30123
accuracy: 0.97061, macro average f1 score: 0.97141
classification report:
             precision    recall  f1-score   support

          0    0.97175   0.96243   0.96707      1251
          1    0.96972   0.97509   0.97239      1445
          2    0.97032   0.97926   0.97477       434

avg / total    0.97061   0.97061   0.97060      3130

epochs without improvement: 19
----------------------------------------------------
epoch: 40, step: 1961
training loss: 0.10319, validation loss: 0.30150
accuracy: 0.97061, macro average f1 score: 0.97037
classification report:
             precision    recall  f1-score   support

          0    0.96582   0.97122   0.96851      1251
          1    0.97431   0.97093   0.97262      1445
          2    0.97222   0.96774   0.96998       434

avg / total    0.97062   0.97061   0.97061      3130

epochs without improvement: 20
----------------------------------------------------
epoch: 41, step: 2010
training loss: 0.09200, validation loss: 0.29029
accuracy: 0.97093, macro average f1 score: 0.97113
classification report:
             precision    recall  f1-score   support

          0    0.96803   0.96803   0.96803      1251
          1    0.97301   0.97301   0.97301      1445
          2    0.97235   0.97235   0.97235       434

avg / total    0.97093   0.97093   0.97093      3130

epochs without improvement: 21
----------------------------------------------------
epoch: 42, step: 2059
training loss: 0.10527, validation loss: 0.29729
accuracy: 0.97061, macro average f1 score: 0.97139
classification report:
             precision    recall  f1-score   support

          0    0.97949   0.95444   0.96680      1251
          1    0.96334   0.98201   0.97258      1445
          2    0.97032   0.97926   0.97477       434

avg / total    0.97076   0.97061   0.97058      3130

epochs without improvement: 22
----------------------------------------------------
epoch: 43, step: 2108
training loss: 0.09217, validation loss: 0.27279
accuracy: 0.97061, macro average f1 score: 0.97117
classification report:
             precision    recall  f1-score   support

          0    0.97254   0.96243   0.96746      1251
          1    0.96972   0.97509   0.97239      1445
          2    0.96811   0.97926   0.97365       434

avg / total    0.97062   0.97061   0.97060      3130

epochs without improvement: 23
----------------------------------------------------
epoch: 44, step: 2157
training loss: 0.08939, validation loss: 0.29139
accuracy: 0.96933, macro average f1 score: 0.96889
classification report:
             precision    recall  f1-score   support

          0    0.97251   0.96163   0.96704      1251
          1    0.96838   0.97509   0.97172      1445
          2    0.96347   0.97235   0.96789       434

avg / total    0.96935   0.96933   0.96932      3130

epochs without improvement: 24
----------------------------------------------------
epoch: 45, step: 2206
training loss: 0.08584, validation loss: 0.27943
accuracy: 0.97061, macro average f1 score: 0.97038
classification report:
             precision    recall  f1-score   support

          0    0.97640   0.95923   0.96774      1251
          1    0.96783   0.97855   0.97316      1445
          2    0.96364   0.97696   0.97025       434

avg / total    0.97068   0.97061   0.97059      3130

epochs without improvement: 25
----------------------------------------------------
epoch: 46, step: 2255
training loss: 0.07981, validation loss: 0.32042
accuracy: 0.96422, macro average f1 score: 0.96384
classification report:
             precision    recall  f1-score   support

          0    0.96022   0.96483   0.96252      1251
          1    0.97532   0.95709   0.96612      1445
          2    0.94066   0.98618   0.96288       434

avg / total    0.96448   0.96422   0.96423      3130

epochs without improvement: 26
----------------------------------------------------
epoch: 47, step: 2304
training loss: 0.08660, validation loss: 0.32120
accuracy: 0.97125, macro average f1 score: 0.97010
classification report:
             precision    recall  f1-score   support

          0    0.97733   0.96483   0.97104      1251
          1    0.96589   0.97993   0.97286      1445
          2    0.97203   0.96083   0.96640       434

avg / total    0.97131   0.97125   0.97124      3130

epochs without improvement: 27
----------------------------------------------------
epoch: 48, step: 2353
training loss: 0.07706, validation loss: 0.30502
accuracy: 0.96997, macro average f1 score: 0.96992
classification report:
             precision    recall  f1-score   support

          0    0.98191   0.95444   0.96798      1251
          1    0.96138   0.98201   0.97159      1445
          2    0.96575   0.97465   0.97018       434

avg / total    0.97019   0.96997   0.96995      3130

epochs without improvement: 28
----------------------------------------------------
epoch: 49, step: 2402
training loss: 0.08381, validation loss: 0.28310
accuracy: 0.97316, macro average f1 score: 0.97342
classification report:
             precision    recall  f1-score   support

          0    0.97191   0.96803   0.96996      1251
          1    0.97642   0.97439   0.97541      1445
          2    0.96606   0.98387   0.97489       434

avg / total    0.97318   0.97316   0.97316      3130

epochs without improvement: 29
----------------------------------------------------
epoch: 50, step: 2451
training loss: 0.07477, validation loss: 0.29520
accuracy: 0.97348, macro average f1 score: 0.97389
classification report:
             precision    recall  f1-score   support

          0    0.98277   0.95763   0.97004      1251
          1    0.96735   0.98408   0.97564      1445
          2    0.96825   0.98387   0.97600       434

avg / total    0.97364   0.97348   0.97345      3130

epochs without improvement: 30
----------------------------------------------------
epoch: 51, step: 2500
training loss: 0.07386, validation loss: 0.30802
accuracy: 0.96901, macro average f1 score: 0.97069
classification report:
             precision    recall  f1-score   support

          0    0.96332   0.96563   0.96447      1251
          1    0.97354   0.96747   0.97050      1445
          2    0.97045   0.98387   0.97712       434

avg / total    0.96903   0.96901   0.96901      3130

epochs without improvement: 31
----------------------------------------------------
epoch: 52, step: 2549
training loss: 0.07539, validation loss: 0.31077
accuracy: 0.97188, macro average f1 score: 0.97316
classification report:
             precision    recall  f1-score   support

          0    0.97796   0.95763   0.96769      1251
          1    0.96658   0.98062   0.97355      1445
          2    0.97267   0.98387   0.97824       434

avg / total    0.97197   0.97188   0.97186      3130

epochs without improvement: 32
----------------------------------------------------
epoch: 53, step: 2598
training loss: 0.06904, validation loss: 0.34114
accuracy: 0.96933, macro average f1 score: 0.96910
classification report:
             precision    recall  f1-score   support

          0    0.98266   0.95124   0.96669      1251
          1    0.95889   0.98478   0.97166      1445
          2    0.96782   0.97005   0.96893       434

avg / total    0.96963   0.96933   0.96930      3130

epochs without improvement: 33
----------------------------------------------------
epoch: 54, step: 2647
training loss: 0.06748, validation loss: 0.31068
accuracy: 0.97284, macro average f1 score: 0.97267
classification report:
             precision    recall  f1-score   support

          0    0.98042   0.96083   0.97053      1251
          1    0.96922   0.98062   0.97489      1445
          2    0.96380   0.98157   0.97260       434

avg / total    0.97295   0.97284   0.97283      3130

epochs without improvement: 34
----------------------------------------------------
epoch: 55, step: 2696
training loss: 0.07094, validation loss: 0.37862
accuracy: 0.96070, macro average f1 score: 0.96192
classification report:
             precision    recall  f1-score   support

          0    0.94618   0.96962   0.95776      1251
          1    0.97040   0.95294   0.96159      1445
          2    0.97203   0.96083   0.96640       434

avg / total    0.96095   0.96070   0.96073      3130

epochs without improvement: 35
----------------------------------------------------
epoch: 56, step: 2745
training loss: 0.06588, validation loss: 0.36445
accuracy: 0.96773, macro average f1 score: 0.96759
classification report:
             precision    recall  f1-score   support

          0    0.97014   0.96083   0.96546      1251
          1    0.96441   0.97509   0.96972      1445
          2    0.97209   0.96313   0.96759       434

avg / total    0.96776   0.96773   0.96772      3130

epochs without improvement: 36
----------------------------------------------------
