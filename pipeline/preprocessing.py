"""Preprocess and merge MAT-files into one dataset."""
import argparse
import os
import pickle
import shutil
from sys import exit

import numpy as np
import pandas as pd
from PIL import Image
from scipy import io
from scipy import ndimage
from scipy import stats

INTENSITY_SAMPLES = 100000  # Number of pixel intensity samples used for the KS test


def load_dataset(mat_files):
    """Load and merge MAT-files."""
    data_index = []
    data_dict = {"label": [], "img": [], "eqsiz": []}
    for i in range(len(mat_files)):
        print("File: {}".format(mat_files[i]))
        # Extract MATLAB variable name
        var = io.whosmat(mat_files[i])[0][0]
        # Extract data
        mat = io.loadmat(mat_files[i], squeeze_me=True, struct_as_record=False)
        ids = mat[var].prtclID
        imgs = mat[var].prtclIm
        labels = mat[var].cpType
        for j, (id_, img, label) in enumerate(zip(ids, imgs, labels)):
            if label == "Artifact":
                label = 0
            elif label == "Particle_round":
                label = 1
            elif label == "Particle_nubbly":
                label = 2
            else:
                label = 0
            # Check for missing IDs
            if isinstance(id_, np.ndarray):
                id_ = "no_id_{}".format(j + 1)
                print("prtclID missing in row {}, substituting with '{}'".format(j + 1, id_))
            # Check for duplicate IDs
            if id_ in data_index:
                print("ID {} not unique".format(id_))
            data_index.append(id_)
            data_dict["label"].append(label)
            data_dict["img"].append(img)
            # Extract the radius (micrometers) from the particle ID
            try:
                eqsiz = float(id_.split("+")[-1])
            except ValueError:
                print("eqsiz is not present in ID, substituting with 0")
                eqsiz = 0
            data_dict["eqsiz"].append(eqsiz)
    
    merged_dataset = pd.DataFrame(data_dict, index=data_index)
    
    return merged_dataset


def preprocess_img(img, img_size=-1):
    """Preprocess an image.
    
    The pixels of the image with NaN value are replaced with zeros. If `img_size` is not negative, the image is then
    zero-padded to square shape and then scaled to `img_size`x`img_size`.
    
    Args:
        img: An image array.
        img_size: Dimensions of the output image (`img_size`x`img_size`), use -1 to keep the original dimensions.
        
    Returns:
        The preprocessed image array.
    """
    # Replace NaN entries with 0
    img = np.nan_to_num(img)
    if img_size < 0:
        # Keep original size
        prep_img = img
    else:
        # Pad and scale images
        max_dim = max(img.shape)
        pad_shape = (max_dim - img.shape[0], max_dim - img.shape[1])
        pad_h_t = pad_shape[0]//2
        pad_h_b = pad_shape[0]//2 + pad_shape[0]%2
        pad_w_l = pad_shape[1]//2
        pad_w_r = pad_shape[1]//2 + pad_shape[1]%2
        square_img = np.pad(img, ((pad_h_t, pad_h_b), (pad_w_l, pad_w_r)), "constant", constant_values=0)
        prep_img = ndimage.zoom(square_img, img_size/max_dim)
    
    return prep_img


def sample_dataset(dataset, samples_n, val_pct, ks_test=False):
    """Draw a training-validation sample.
    
    The training-validation sample is split in training and validation sets and the rest of the dataset is used as test
    set.
    
    Args:
        dataset: A DataFrame containing the dataset.
        samples_n: The size of the training-validation sample.
        val_pct: The percentage of the training-validation sample to be used for validation.
        ks_test: Optionally use the Kolmogorov-Smirnov test when sampling, disabled by default. The KS test ensures that
            that the pixel intensity distribution is similar to the full dataset distribution.
    
    Returns:
        The training, validation and test sets.
    """
    samples = None
    if not ks_test:
        # Use uniform sampling
        samples = dataset.sample(n=samples_n)
    else:
        # Use the Kolmogorov-Smirnov (KS) test for sampling
        intensities = [x for img_abs in dataset["img_abs"] for x in img_abs.flatten()]
        # Sample pixel intensities to speed up KS tests
        intensities = np.random.choice(intensities, INTENSITY_SAMPLES)
        p_value = 0
        i = 0
        # Resample until the KS test indicates that the pixel intensity distribution for the sample is similar to that
        # of the full dataset
        while p_value < 0.05:
            samples = dataset.sample(n=samples_n)
            samples_intensities = [x for img_abs in samples["img_abs"] for x in img_abs.flatten()]
            # Compute the KS statistic and the corresponding p-value
            _, p_value = stats.ks_2samp(intensities, samples_intensities)
            i += 1
    
    test_set = dataset.drop(samples.index)
    if val_pct == 0:
        val_set = pd.DataFrame()
        train_set = samples
    else:
        val_set = samples.sample(frac=val_pct)
        train_set = samples.drop(val_set.index)
    
    return train_set, val_set, test_set


def train_val_test_split(dataset, val_pct, test_pct):
    """Split the dataset into training, validation and test sets."""
    test_set = dataset.sample(frac=test_pct)
    trainval_set = dataset.drop(test_set.index)
    # Adjust the validation percentage, as it corresponds to the full dataset: val_pct' = val_pct/(1 - test_pct)
    val_set = trainval_set.sample(frac=val_pct/(1 - test_pct))
    train_set = trainval_set.drop(val_set.index)
    
    return train_set, val_set, test_set


def create_empty_dir(dir_):
    """Create an empty directory."""
    os.makedirs(dir_, exist_ok=True)
    shutil.rmtree(dir_)
    os.makedirs(dir_, exist_ok=True)


def visualize_dataset(dataset, vis_dir):
    """Create and save the amplitude and phase images for each data point in the dataset."""
    for id_, row in dataset.iterrows():
        label = row["label"]
        img_abs = row["img_abs"]
        img_ang = row["img_ang"]
        # Scale pixel values to [0, 255]
        Image.fromarray(img_abs*255/np.max(img_abs)).convert("RGB").save(
            "{}/{}__{}__abs.png".format(vis_dir, label, id_))
        Image.fromarray(img_ang*255/np.max(img_ang)).convert("RGB").save(
            "{}/{}__{}__ang.png".format(vis_dir, label, id_))


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--mat_files", nargs="+", type=str, default=[], help="MAT-files files to be preprocessed")
    parser.add_argument("--prep_dir", type=str, default="prep_data",
                        help="directory to store the preprocessed dataset")
    parser.add_argument("--img_size", type=int, default=32,
                        help="fixed size <img_size>x<img_size> for all images, -1 keeps the original dimensions")
    parser.add_argument("--samples", type=int, default=0,
                        help="extract samples for the training and validation sets from the dataset, "
                             "0 disables sampling")
    parser.add_argument("--ks_test", action="store_true",
                        help="resample until the sample pixel intensity distribution is similar to the full dataset "
                             "distribution according to the Kolmogorov-Smirnov test")
    parser.add_argument("--split", action="store_true", help="split the dataset in train, validation and test sets")
    parser.add_argument("--val_pct", type=float, default=0.2, help="percentage of data used for the validation set")
    parser.add_argument("--test_pct", type=float, default=0.2, help="percentage of data used for test set")
    parser.add_argument("--seed", type=int, default=None,
                        help="random seed used for sampling and/or splitting the dataset")
    parser.add_argument("--suffix", type=str, help="suffix for the created files and directories")
    parser.add_argument("--vis", action="store_true", help="visualize the dataset")
    parser.add_argument("--vis_dir", type=str, default="visualizations",
                        help="directory to store the visualizations of the datasets")
    
    return parser.parse_args()


def main(args):
    if not args.mat_files:
        print("No MAT-files files were provided")
        exit(-1)
    suffix = "" if args.suffix is None else "_" + args.suffix
    np.random.seed(args.seed)
    
    print("Loading and merging files")
    merged_dataset = load_dataset(args.mat_files)
    
    print("Preprocessing the dataset")
    merged_dataset["volume"] = merged_dataset["eqsiz"].map(lambda r: 4.0/3.0*np.pi*r**3)
    merged_dataset["shape"] = merged_dataset["img"].map(lambda img: img.shape)
    merged_dataset["size"] = merged_dataset["shape"].map(lambda shape: max(shape))
    merged_dataset["img_abs"] = merged_dataset["img"].map(lambda img: preprocess_img(np.absolute(img), args.img_size))
    merged_dataset["img_ang"] = merged_dataset["img"].map(lambda img: preprocess_img(np.angle(img), args.img_size))
    
    train_set = val_set = test_set = None
    if args.samples > 0:
        print("Sampling from the dataset")
        train_set, val_set, test_set = sample_dataset(merged_dataset, args.samples, args.val_pct, args.ks_test)
    elif args.split:
        print("Splitting the dataset into train, validation and test sets")
        train_set, val_set, test_set = train_val_test_split(merged_dataset, args.val_pct, args.test_pct)
    
    print("Saving the preprocessed dataset")
    os.makedirs(args.prep_dir, exist_ok=True)
    if args.samples > 0 or args.split:
        train_set.to_pickle("{}/train{}.pkl".format(args.prep_dir, suffix))
        val_set.to_pickle("{}/val{}.pkl".format(args.prep_dir, suffix))
        test_set.to_pickle("{}/test{}.pkl".format(args.prep_dir, suffix))
    else:
        merged_dataset.to_pickle("{}/dataset{}.pkl".format(args.prep_dir, suffix))
    
    if args.vis:
        print("Visualizing the dataset")
        os.makedirs(args.vis_dir, exist_ok=True)
        vis_dir = "{}/visualization{}".format(args.vis_dir, suffix)
        create_empty_dir(vis_dir)
        if args.samples > 0 or args.split:
            create_empty_dir("{}/train".format(vis_dir))
            visualize_dataset(train_set, "{}/train".format(vis_dir))
            create_empty_dir("{}/validation".format(vis_dir))
            visualize_dataset(val_set, "{}/validation".format(vis_dir))
            create_empty_dir("{}/test".format(vis_dir))
            visualize_dataset(test_set, "{}/test".format(vis_dir))
        else:
            visualize_dataset(merged_dataset, vis_dir)


if __name__ == "__main__":
    main(parse_args())
