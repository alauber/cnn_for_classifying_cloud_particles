"""Train, fine-tune, test and make predictions with DCNN models."""
import argparse
import os
import pickle
import time
from math import sqrt

import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score

NUM_LABELS = 3
NUM_CHANNELS = 2


def extract_data_labels(filename):
    """Load a dataset and extract the IDs, data and labels."""
    dataset = pd.read_pickle(filename)
    ids = []
    data = []  # 4D array [image_index, x, y, channel]
    labels = []  # 2D array [image_index, one_hot_label]
    for id_, row in dataset.iterrows():
        label = row["label"]
        img_abs = row["img_abs"]
        img_ang = row["img_ang"]
        ids.append(id_)
        data.append(np.stack((img_abs, img_ang), 2))
        one_hot_label = [0, 0, 0]
        one_hot_label[label] = 1
        labels.append(one_hot_label)
    
    return ids, np.asarray(data), np.asarray(labels)


def print_and_log(message, log_file):
    """Print a message to standard output and append it to a logfile."""
    print(message)
    log_file.write(message + "\n")
    log_file.flush()


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--train", action="store_true", help="train the model")
    parser.add_argument("--resume_training", action="store_true", help="restore the model and resume training")
    parser.add_argument("--val", action="store_true", help="evaluate on validation set while training")
    parser.add_argument("--test", action="store_true", help="evaluate on test set without re-training")
    parser.add_argument("--predict", action="store_true", help="make predictions for the <pred_set>")
    parser.add_argument("--train_set", type=str, help="file containing the train set")
    parser.add_argument("--val_set", type=str, help="file containing the validation set")
    parser.add_argument("--test_set", type=str, help="file containing the test set")
    parser.add_argument("--pred_set", type=str, help="file containing the set on which predictions are to be made")
    parser.add_argument("--log_dir", type=str, default="logfiles",
                        help="directory containing the logfiles and summaries")
    parser.add_argument("--model_dir", type=str, default="models", help="directory containing the model")
    parser.add_argument("--pred_dir", type=str, default="predictions", help="directory containing the predictions")
    parser.add_argument("--img_size", type=int, default=32, help="fixed size <img_size>x<img_size> for all images")
    parser.add_argument("--augment", action="store_true", help="perform data augmentation during training")
    parser.add_argument("--batch_size", type=int, default=256, help="number of images per batch")
    parser.add_argument("--max_epochs", type=int, default=200, help="maximum number of epochs for training")
    parser.add_argument("--early_stopping_epochs", type=int, default=10,
                        help="number of epochs without improvement to stop training, -1 disables early stopping")
    parser.add_argument("--learning_rate", type=float, default=0.001, help="learning rate for the optimizer")
    parser.add_argument("--batchnorm", action="store_true", help="perform batch normalization in every layer")
    parser.add_argument("--residual", action="store_true",
                        help="add dense residual connections in all convolutional blocks")
    parser.add_argument("--dropout", type=float, default=1,
                        help="dropout keep probability for the fully connected layers, 1 disables dropout")
    parser.add_argument("--l2_reg", type=float, default=0,
                        help="L2 regularization hyperparameter, 0 disables L2 regularization")
    parser.add_argument("--seed", type=int, default=None,
                        help="random seed used for training, the determinism of the results also depends on the "
                             "TensorFlow version")
    parser.add_argument("--suffix", type=str, default=None, help="suffix for the created files and directories")
    
    return parser.parse_args()


def main(args):
    np.random.seed(args.seed)
    tf.set_random_seed(args.seed)
    
    data_node = tf.placeholder(tf.float32, shape=(None, args.img_size, args.img_size, NUM_CHANNELS), name="data_node")
    labels_node = tf.placeholder(tf.float32, shape=(None, NUM_LABELS), name="labels_node")
    keep_prob = tf.placeholder(tf.float32, name="keep_prob")
    training = tf.placeholder(tf.bool, name="training")
    weights = tf.placeholder(tf.float32, shape=NUM_LABELS)
    batch_weights = tf.reduce_sum(labels_node*weights, axis=1)

    def random_apply_function(fn, img):
        """Augment an image by applying a transformation function with 50% probability."""
        rand = tf.random_uniform([], 0, 1.0)
        pred = tf.less(rand, 0.5)
    
        return tf.cond(pred, lambda: fn(img), lambda: img)
    
    def augment_image(img):
        """Augment an image by applying transformations with 50% probability."""
        img = tf.image.random_flip_up_down(img)
        img = tf.image.random_flip_left_right(img)
        img = random_apply_function(tf.image.rot90, img)
        img = random_apply_function(tf.image.transpose_image, img)
        
        return img
    
    # Augmentation and batch normalization on the input
    data_input = data_node
    if args.augment:
        data_input = tf.cond(training, lambda: tf.map_fn(augment_image, data_node), lambda: data_node)
    if args.batchnorm:
        data_input = tf.layers.batch_normalization(data_input, training=training, fused=True)
    
    def weight_variable(name, shape):
        """Create a weight variable."""
        w = tf.get_variable(name, shape=shape, initializer=tf.contrib.layers.xavier_initializer())
        return w
    
    def bias_variable(name, shape):
        """Create a bias variable."""
        b = tf.get_variable(name, initializer=tf.constant(0.1, shape=shape))
        return b
    
    def conv_layer(x, w, b, name=None):
        """Create a convolutional layer."""
        conv = tf.nn.bias_add(tf.nn.conv2d(x, w, strides=[1, 1, 1, 1], padding="SAME"), b, name=name)
        if args.batchnorm:
            conv = tf.layers.batch_normalization(conv, training=training, fused=True)
        relu = tf.nn.relu(conv)
        return relu
    
    def pool_layer(x, name=None):
        """Create a max pooling layer."""
        pool = tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding="SAME", name=name)
        if args.batchnorm:
            pool = tf.layers.batch_normalization(pool, training=training, fused=True)
        return pool
    
    def final_pool_dim(initial_dim, num_pool):
        """Calculate the spatial dimensions of the representation after the last pooling layer."""
        output_dim = initial_dim
        for i in range(num_pool):
            output_dim = int(np.ceil(output_dim/2))
        return output_dim
    
    def fc_layer(x, w, b, name=None):
        """Create a fully connected layer."""
        fc = tf.nn.bias_add(tf.matmul(x, w), b, name=name)
        if args.batchnorm:
            fc = tf.layers.batch_normalization(fc, training=training, fused=True)
        relu = tf.nn.relu(fc)
        drop = tf.nn.dropout(relu, keep_prob)
        return drop
    
    def model(data):
        """Create the DCNN model."""
        w_1_1 = weight_variable("w_1_1", [3, 3, NUM_CHANNELS, 64])
        w_1_2 = weight_variable("w_1_2", [3, 3, 64, 64])
        w_2_1 = weight_variable("w_2_1", [3, 3, 64, 128])
        w_2_2 = weight_variable("w_2_2", [3, 3, 128, 128])
        w_3_1 = weight_variable("w_3_1", [3, 3, 128, 256])
        w_3_2 = weight_variable("w_3_2", [3, 3, 256, 256])
        w_3_3 = weight_variable("w_3_3", [3, 3, 256, 256])
        w_4_1 = weight_variable("w_4_1", [3, 3, 256, 512])
        w_4_2 = weight_variable("w_4_2", [3, 3, 512, 512])
        w_4_3 = weight_variable("w_4_3", [3, 3, 512, 512])
        b_1_1 = bias_variable("b_1_1", [64])
        b_1_2 = bias_variable("b_1_2", [64])
        b_2_1 = bias_variable("b_2_1", [128])
        b_2_2 = bias_variable("b_2_2", [128])
        b_3_1 = bias_variable("b_3_1", [256])
        b_3_2 = bias_variable("b_3_2", [256])
        b_3_3 = bias_variable("b_3_3", [256])
        b_4_1 = bias_variable("b_4_1", [512])
        b_4_2 = bias_variable("b_4_2", [512])
        b_4_3 = bias_variable("b_4_3", [512])
        fc_input_dim = (final_pool_dim(args.img_size, 4))**2*512
        w_fc1 = weight_variable("w_fc1", [fc_input_dim, 1024])
        b_fc1 = bias_variable("b_fc1", [1024])
        w_fc2 = weight_variable("w_fc2", [1024, 128])
        b_fc2 = bias_variable("b_fc2", [128])
        w_fc3 = weight_variable("w_fc3", [128, NUM_LABELS])
        b_fc3 = bias_variable("b_fc3", [NUM_LABELS])
        
        if not args.residual:
            conv_1_1 = conv_layer(data, w_1_1, b_1_1, name="conv_1_1")
            conv_1_2 = conv_layer(conv_1_1, w_1_2, b_1_2, name="conv_1_2")
            
            pool_1 = pool_layer(conv_1_2, name="pool_1")
            
            conv_2_1 = conv_layer(pool_1, w_2_1, b_2_1, name="conv_2_1")
            conv_2_2 = conv_layer(conv_2_1, w_2_2, b_2_2, name="conv_2_2")
            
            pool_2 = pool_layer(conv_2_2, name="pool_2")
            
            conv_3_1 = conv_layer(pool_2, w_3_1, b_3_1, name="conv_3_1")
            conv_3_2 = conv_layer(conv_3_1, w_3_2, b_3_2, name="conv_3_2")
            conv_3_3 = conv_layer(conv_3_2, w_3_3, b_3_3, name="conv_3_3")
            
            pool_3 = pool_layer(conv_3_3, name="pool_3")
            
            conv_4_1 = conv_layer(pool_3, w_4_1, b_4_1, name="conv_4_1")
            conv_4_2 = conv_layer(conv_4_1, w_4_2, b_4_2, name="conv_4_2")
            conv_4_3 = conv_layer(conv_4_2, w_4_3, b_4_3, name="conv_4_3")
            
            pool_4 = pool_layer(conv_4_3, name="pool_4")
        else:
            conv_1_1 = conv_layer(data, w_1_1, b_1_1, name="conv_1_1")
            conv_1_2 = conv_layer(conv_1_1, w_1_2, b_1_2, name="conv_1_2")
            
            pool_1 = pool_layer(conv_1_1 + conv_1_2, name="pool_1")
            
            conv_2_1 = conv_layer(pool_1, w_2_1, b_2_1, name="conv_2_1")
            conv_2_2 = conv_layer(conv_2_1, w_2_2, b_2_2, name="conv_2_2")
            
            pool_2 = pool_layer(conv_2_1 + conv_2_2, name="pool_2")
            
            conv_3_1 = conv_layer(pool_2, w_3_1, b_3_1, name="conv_3_1")
            conv_3_2 = conv_layer(conv_3_1, w_3_2, b_3_2, name="conv_3_2")
            conv_3_3 = conv_layer(conv_3_1 + conv_3_2, w_3_3, b_3_3, name="conv_3_3")
            
            pool_3 = pool_layer(conv_3_1 + conv_3_2 + conv_3_3, name="pool_3")
            
            conv_4_1 = conv_layer(pool_3, w_4_1, b_4_1, name="conv_4_1")
            conv_4_2 = conv_layer(conv_4_1, w_4_2, b_4_2, name="conv_4_2")
            conv_4_3 = conv_layer(conv_4_1 + conv_4_2, w_4_3, b_4_3, name="conv_4_3")
            
            pool_4 = pool_layer(conv_4_1 + conv_4_2 + conv_4_3, name="pool_4")
        
        # Reshape the 4D CNN feature map into 2D tensor to be feed to the first fully connected layer
        pool_shape = pool_4.get_shape().as_list()
        reshape = tf.reshape(pool_4, [-1, pool_shape[1]*pool_shape[2]*pool_shape[3]])
        fc_1 = fc_layer(reshape, w_fc1, b_fc1, name="fc_1")
        fc_2 = fc_layer(fc_1, w_fc2, b_fc2, name="fc_2")
        fc_3 = tf.nn.bias_add(tf.matmul(fc_2, w_fc3), b_fc3, name="fc_3")
        
        regularizers = (
            tf.nn.l2_loss(w_1_1) + tf.nn.l2_loss(w_1_2) + tf.nn.l2_loss(w_2_1) + tf.nn.l2_loss(w_2_2) +
            tf.nn.l2_loss(w_3_1) + tf.nn.l2_loss(w_3_2) + tf.nn.l2_loss(w_3_3) + tf.nn.l2_loss(w_4_1) +
            tf.nn.l2_loss(w_4_2) + tf.nn.l2_loss(w_4_3) + tf.nn.l2_loss(w_fc1) + tf.nn.l2_loss(w_fc2) +
            tf.nn.l2_loss(w_fc3)
        )
        
        return fc_3, regularizers
    
    # Cross-entropy loss
    logits, regularizers = model(data_input)
    # FIXME: Sometimes the loss is NaN for small training size and the network does not train
    loss = tf.reduce_mean(tf.losses.softmax_cross_entropy(labels_node, logits, weights=batch_weights))
    
    # Add the L2 regularization term to the loss
    loss = tf.add(loss, args.l2_reg*regularizers, name="loss")
    
    # Use Adam for the optimization
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    with tf.control_dependencies(update_ops):
        optimizer = tf.train.AdamOptimizer(args.learning_rate).minimize(loss)
    
    # Predictions for the minibatch, validation set and test set
    softmax_output = tf.nn.softmax(logits, name="softmax")
    model_predictions = tf.argmax(softmax_output, axis=1, name="model_predictions")
    
    # Calculate accuracy
    labels_argmax = tf.argmax(labels_node, 1)
    accuracy_train = tf.reduce_mean(tf.cast(tf.equal(labels_argmax, model_predictions), tf.float32),
                                    name="accuracy_train")
    
    # Training summaries
    loss_train_summary = tf.summary.scalar("loss_training", loss)
    accuracy_train_summary = tf.summary.scalar("accuracy_training", accuracy_train)
    gradient_norm = tf.global_norm(tf.gradients(loss, tf.trainable_variables()))
    gradient_norm_summary = tf.summary.scalar("gradient_norm", gradient_norm)
    
    summary_train = tf.summary.merge([loss_train_summary, accuracy_train_summary, gradient_norm_summary])
    
    # Accumulate the validation summaries
    loss_val = tf.placeholder(tf.float32, name="loss_val")
    loss_val_summary = tf.summary.scalar("loss_validation", loss_val)
    accuracy_val = tf.placeholder(tf.float32, name="accuracy_val")
    accuracy_val_summary = tf.summary.scalar("accuracy_validation", accuracy_val)
    f1_avg_val = tf.placeholder(tf.float32, name="f1_avg_val")
    f1_avg_val_summary = tf.summary.scalar("f1_average_validation", f1_avg_val)
    
    summary_val = tf.summary.merge([loss_val_summary, accuracy_val_summary, f1_avg_val_summary])
    
    def evaluate_dataset(data, labels, sess):
        """Evaluate the model on data."""
        set_size = data.shape[0]
        loss_validation = 0
        predictions = []
        for step in range(int(np.ceil(set_size/args.batch_size))):
            offset = step*args.batch_size
            if offset + args.batch_size < set_size:
                batch_data = data[offset:offset + args.batch_size, :, :, :]
                batch_labels = labels[offset:offset + args.batch_size]
            else:
                batch_data = data[offset:, :, :, :]
                batch_labels = labels[offset:]
            
            feed_dict = {data_node: batch_data, labels_node: batch_labels, weights: class_weights, keep_prob: 1.0,
                         training: False}
            l, batch_predictions = sess.run([loss, model_predictions], feed_dict=feed_dict)
            loss_validation += l
            predictions += batch_predictions.tolist()
        loss_validation = loss_validation/(set_size//args.batch_size)
        
        # Calculate accuracy and F1 score
        labels_argmax_all = np.argmax(labels, 1)
        accuracy = accuracy_score(labels_argmax_all, predictions)
        cmat = confusion_matrix(labels_argmax_all, predictions)
        f1_avg = f1_score(labels_argmax_all, predictions, average="macro")
        report = classification_report(labels_argmax_all, predictions, digits=5)
        feed_dict = {loss_val: loss_validation, accuracy_val: accuracy, f1_avg_val: f1_avg}
        summary = sess.run(summary_val, feed_dict=feed_dict)
        
        return summary, predictions, loss_validation, accuracy, f1_avg, cmat, report
    
    def predict(data, sess):
        """Make predictions for data using the model."""
        set_size = data.shape[0]
        predictions = []
        for step in range(int(np.ceil(set_size/args.batch_size))):
            offset = step*args.batch_size
            if offset + args.batch_size < set_size:
                batch_data = data[offset:offset + args.batch_size, :, :, :]
            else:
                batch_data = data[offset:, :, :, :]
            
            feed_dict = {data_node: batch_data, keep_prob: 1.0, training: False}
            batch_predictions = sess.run(model_predictions, feed_dict=feed_dict)
            predictions += batch_predictions.tolist()
        
        return predictions
    
    # Add ops to save and restore all the variables
    saver = tf.train.Saver()

    args.suffix = "" if args.suffix is None else "_" + args.suffix
    os.makedirs(args.log_dir, exist_ok=True)
    
    # Create a local session to run this computation
    with tf.Session() as sess, open("{}/dcnn_log{}.txt".format(args.log_dir, args.suffix), "w") as log_file:
        print_and_log("args: {}\n".format(args), log_file)
        class_weights = np.ones(NUM_LABELS)
        
        # Train
        if args.train:
            print("Train")
            print("Loading train set")
            _, train_data, train_labels = extract_data_labels(args.train_set)
            # Calculate the class weights for the loss function
            class_weights = np.sum(train_labels)/np.sum(train_labels, axis=0)
            
            if args.val:
                print("Loading validation set")
                _, val_data, val_labels = extract_data_labels(args.val_set)
            
            summary_writer = tf.summary.FileWriter("{}/dcnn_summaries{}".format(args.log_dir, args.suffix), sess.graph)
            
            if args.resume_training:
                saver.restore(sess, args.model_dir + "/model.ckpt")
                print("Model restored")
                parent_dir, _ = os.path.split(os.path.normpath(args.model_dir))
                args.model_dir = "{}/dcnn_model{}".format(parent_dir, args.suffix)
                os.makedirs(args.model_dir, exist_ok=True)
            else:
                init = tf.global_variables_initializer()
                sess.run(init)
                print("Model initialized")
                os.makedirs(args.model_dir, exist_ok=True)
                args.model_dir = "{}/dcnn_model{}".format(args.model_dir, args.suffix)
                os.makedirs(args.model_dir, exist_ok=True)
            
            train_size = train_labels.shape[0]
            train_indices = range(train_size)
            min_loss_validation = float("inf")
            epochs_without_improvement = 0
            global_step = 1
            loss_training = 0
            for epoch in range(1, args.max_epochs + 1):
                # Permute training indices
                perm_indices = np.random.permutation(train_indices)
                for step in range(int(np.ceil(train_size/args.batch_size))):
                    offset = step*args.batch_size
                    if offset + args.batch_size < train_size:
                        batch_indices = perm_indices[offset:offset + args.batch_size]
                    else:
                        # Include the last (possibly not full) training batch
                        batch_indices = perm_indices[offset:]
                    batch_data = train_data[batch_indices, :, :, :]
                    batch_labels = train_labels[batch_indices]
                    # Train on one batch of training data
                    feed_dict = {data_node: batch_data, labels_node: batch_labels, weights: class_weights,
                                 keep_prob: args.dropout, training: True}
                    summary, _, l = sess.run([summary_train, optimizer, loss], feed_dict=feed_dict)
                    summary_writer.add_summary(summary, global_step)
                    loss_training += l
                    global_step += 1
                # End of epoch
                loss_training = loss_training/(train_size//args.batch_size)
                
                if not args.val:
                    message = "epoch: {}, step: {}\n".format(epoch, global_step) + \
                              "training loss: {:.5f}\n".format(loss_training) + \
                              "----------------------------------------------------"
                    print_and_log(message, log_file)
                    # Save the model
                    saver.save(sess, args.model_dir + "/model.ckpt")
                else:
                    # Evaluate model on the validation set
                    summary, predictions, loss_validation, accuracy, f1_avg, _, report = evaluate_dataset(val_data,
                                                                                                          val_labels,
                                                                                                          sess)
                    summary_writer.add_summary(summary, global_step)
                    
                    message = "epoch: {}, step: {}\n".format(epoch, global_step) + \
                              "training loss: {:.5f}, validation loss: {:.5f}\n".format(loss_training,
                                                                                        loss_validation) + \
                              "accuracy: {:.5f}, macro average f1 score: {:.5f}\n".format(accuracy, f1_avg) + \
                              "classification report:\n{}".format(report)
                    print_and_log(message, log_file)
                    
                    # Calculate the epochs with no improvement in validation loss for early stopping
                    if epoch == 1 or loss_validation < min_loss_validation:
                        # Save the model
                        saver.save(sess, args.model_dir + "/model.ckpt")
                        min_loss_validation = loss_validation
                        epochs_without_improvement = 0
                        print_and_log("epochs without improvement: {}, model saved".format(epochs_without_improvement),
                                      log_file)
                    else:
                        epochs_without_improvement += 1
                        print_and_log("epochs without improvement: {}".format(epochs_without_improvement), log_file)
                    
                    print_and_log("----------------------------------------------------", log_file)
                    loss_training = 0
                    
                    # Check the early stopping condition
                    if 0 < args.early_stopping_epochs == epochs_without_improvement:
                        break
        
        # Test
        if args.test:
            print("Test")
            saver.restore(sess, args.model_dir + "/model.ckpt")
            print("Model restored")
            
            print("Loading test set")
            test_ids, test_data, test_labels = extract_data_labels(args.test_set)
            
            print("Evaluating model on test set")
            _, predictions, _, accuracy, f1_avg, cmat, report = evaluate_dataset(test_data, test_labels, sess)
            message = "test set evaluation\n" + \
                      "accuracy: {:.5f}, macro average f1 score: {:.5f}\n".format(accuracy, f1_avg) + \
                      "classification report:\n{}".format(report) + \
                      "confusion matrix:\n{}".format(cmat)
            print_and_log(message, log_file)
        
        # Predict
        if args.predict:
            print("Predict")
            saver.restore(sess, args.model_dir + "/model.ckpt")
            print("Model restored")
            
            print("Loading predict data")
            pred_ids, pred_data, _ = extract_data_labels(args.pred_set)
            
            print("Making predictions")
            predictions = predict(pred_data, sess)
            
            print("Saving predictions")
            os.makedirs(args.pred_dir, exist_ok=True)
            with open("{}/dcnn_pred{}.csv".format(args.pred_dir, args.suffix), "w") as pred_file:
                for i in range(len(pred_ids)):
                    pred_file.write("{},{}\n".format(pred_ids[i], predictions[i]))


if __name__ == "__main__":
    main(parse_args())
