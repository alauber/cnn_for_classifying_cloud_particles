The folders contain the experiment logfiles which include the arguments which were used during the experiments (also the used test, training and validation set) and the classification reports after each epoch. The last row includes the confusion matrix for the model being tested on the test dataset.

The folders stand for the following experiments:
FT_merged: trained on four datasets and fine-tuned and tested on a fifth dataset (not used for training). The subfolders have the names of the number of samples which were used for fine-tuning the model and include up to three runs for the five datasets. If the randomly chosen samples did not include all three classes, the fine-tuning did not work and was therefore excluded (less than three runs).

FT_single: trained on a single dataset and fine-tuned and tested on the same or another single dataset. Three runs are available for every possible combination. If the randomly chosen samples did not include all three classes, the fine-tuning did not work and was therefore excluded (less than three runs).

VGG_merged: trained on four datasets and tested on a fifth dataset (not used for training). Three runs for the five datasets are available

VGG_samples: trained on a specific number of samples (name of folder) of one dataset and tested on the same (not with the same samples). The folders include up to three runs for the five datasets. If the randomly chosen samples did not include all three classes, the training did not work and was therefore excluded (less than three runs).

VGG_single: The folder "different_datasets" includes the results of three runs for models being trained on a single dataset and tested on a different dataset. The folder "same_dataset" includes the results of three runs of models being trained on a single dataset and tested on the same dataset (not with the same samples).