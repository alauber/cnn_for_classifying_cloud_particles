Arguments: Namespace(arch='VGG', augment=True, batch_size=256, batchnorm=True, domain_adaptation=False, domain_match=None, dropout_fc=1.0, dropout_in=1.0, early_stopping_steps=10, img_size=32, l2_reg=0, lamb=1.0, learning_rate=0.001, log_dir='logfiles', max_epochs=200, model_dir='models/cnn_cloud_model_20180419-132531_VGG_res_BN_aug_2016_JFJ_r2', no_phase=False, pred_dir='predictions', pred_set='prep_data/test_2016_HOL3G.pkl', predict=True, recording_steps=300, residual=True, resume_training=False, sigma=100.0, suffix='20180419-141359_VGG_res_BN_aug_2016_JFJ_r2_2016_HOL3G', tSNE=False, tSNE_dir='tSNE', targ_set='prep_data/test_classified_32.pkl', test=True, test_set='prep_data/test_2016_HOL3G.pkl', train=False, train_set='prep_data/train_classified_32.pkl', use_class=None, use_shape=False, val=False, val_set='prep_data/val_classified_32.pkl', weigh_volume=False)

test set evaluation
accuracy: 0.70503, macro average f1 score: 0.62139
classification report:
             precision    recall  f1-score   support

          0    0.37315   0.91489   0.53009       799
          1    0.96912   0.67180   0.79353      3504
          2    0.80460   0.40698   0.54054       172

avg / total    0.85639   0.70503   0.73677      4475
confusion matrix:
[[ 731   54   14]
 [1147 2354    3]
 [  81   21   70]]
