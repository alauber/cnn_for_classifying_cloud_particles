Arguments: Namespace(arch='VGG', augment=True, batch_size=256, batchnorm=True, domain_adaptation=False, domain_match=None, dropout_fc=1.0, dropout_in=1.0, early_stopping_steps=10, img_size=32, l2_reg=0, lamb=1.0, learning_rate=0.001, log_dir='logfiles', max_epochs=200, model_dir='models/cnn_cloud_model_20180419-132529_VGG_res_BN_aug_2017_SON_r1', no_phase=False, pred_dir='predictions', pred_set='prep_data/test_2016_HOL3M.pkl', predict=True, recording_steps=300, residual=True, resume_training=False, sigma=100.0, suffix='20180419-141700_VGG_res_BN_aug_2017_SON_r1_2016_HOL3M', tSNE=False, tSNE_dir='tSNE', targ_set='prep_data/test_classified_32.pkl', test=True, test_set='prep_data/test_2016_HOL3M.pkl', train=False, train_set='prep_data/train_classified_32.pkl', use_class=None, use_shape=False, val=False, val_set='prep_data/val_classified_32.pkl', weigh_volume=False)

test set evaluation
accuracy: 0.67465, macro average f1 score: 0.55974
classification report:
             precision    recall  f1-score   support

          0    0.99947   0.59217   0.74371      3168
          1    0.67096   0.98116   0.79694       796
          2    0.07525   0.87356   0.13856        87

avg / total    0.91507   0.67465   0.74117      4051
confusion matrix:
[[1876  373  919]
 [   0  781   15]
 [   1   10   76]]
