Arguments: Namespace(arch='VGG', augment=True, batch_size=256, batchnorm=True, domain_adaptation=False, domain_match=None, dropout_fc=1.0, dropout_in=1.0, early_stopping_steps=10, img_size=32, l2_reg=0, lamb=1.0, learning_rate=0.001, log_dir='logfiles', max_epochs=200, model_dir='models/cnn_cloud_model_20180419-132529_VGG_res_BN_aug_2017_SON_r3', no_phase=False, pred_dir='predictions', pred_set='prep_data/test_2017_SON.pkl', predict=True, recording_steps=300, residual=True, resume_training=False, sigma=100.0, suffix='20180419-141700_VGG_res_BN_aug_2017_SON_r3_2017_SON', tSNE=False, tSNE_dir='tSNE', targ_set='prep_data/test_classified_32.pkl', test=True, test_set='prep_data/test_2017_SON.pkl', train=False, train_set='prep_data/train_classified_32.pkl', use_class=None, use_shape=False, val=False, val_set='prep_data/val_classified_32.pkl', weigh_volume=False)

test set evaluation
accuracy: 0.97920, macro average f1 score: 0.81866
classification report:
             precision    recall  f1-score   support

          0    0.98656   0.97606   0.98128       376
          1    0.32075   1.00000   0.48571        34
          2    0.99883   0.97934   0.98899      3485

avg / total    0.99173   0.97920   0.98385      3895
confusion matrix:
[[ 367    5    4]
 [   0   34    0]
 [   5   67 3413]]
