%Add Folder Functions which is a subfolder of
%CNN_for_Classifying_Cloud_Particles folder
current_path = pwd;
main_folder_name_position = strfind(current_path,'CNN_for_Classifying_Cloud_Particles');
folder_root = current_path(1:main_folder_name_position+34);
folder_path = strcat(folder_root,'\Functions');
addpath(genpath(folder_path));
clear current_path main_folder_name_position folder_path

load('data')
close all
[ax1,ax2,ax3,ax4] = plot_tables(data);
ylim(ax1,[0,1])
ylim(ax2,[0,1])
ylim(ax3,[0 1])
ylim(ax4,[0,1])