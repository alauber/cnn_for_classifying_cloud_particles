%Add Folder Functions which is a subfolder of
%CNN_for_Classifying_Cloud_Particles folder
current_path = pwd;
main_folder_name_position = strfind(current_path,'CNN_for_Classifying_Cloud_Particles');
folder_root = current_path(1:main_folder_name_position+34);
folder_path = strcat(folder_root,'\Functions');
addpath(genpath(folder_path));
clear current_path main_folder_name_position folder_path

datasets = {'HOL3G','HOL3M','JFJ','2016_SON','2017_SON'};
%Fieldnames starting with a number are not valid and are therefore changed
datasets_names = {'HOL3G','HOL3M','JFJ','SON2016','SON2017'};

clear data
files_path = fullfile(folder_root,'\Data\predictions\Tree_SVM\single\*tree_predicted.mat');
data_dt = get_data_from_predictions(files_path);
data.dt = data_dt.different;
files_path = fullfile(folder_root,'Data\predictions\Tree_SVM\single\*SVM_predicted.mat');
data_SVM = get_data_from_predictions(files_path);
data.SVM = data_SVM.different;
M = get_confusion_matrices_from_txt_files(fullfile(folder_root,'\Data\experiment_logfiles\VGG_single\different_dataset'));
data_dcnn = get_metrics_from_matrices_folders(M);
data.DCNN = get_medians_of_metrics(data_dcnn);
M = get_confusion_matrices_from_txt_files(fullfile(folder_root,'\Data\experiment_logfiles\FT_single\256'));
data_dcnn = get_metrics_from_matrices_folders(M);
data.fine_tuned = get_medians_of_metrics(data_dcnn);
clear data_dcnn data_dt data_SVM datasets datasets_names files_path folder_root M


