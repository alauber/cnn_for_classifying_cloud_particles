%Add Folder Functions which is a subfolder of
%CNN_for_Classifying_Cloud_Particles folder
current_path = pwd;
main_folder_name_position = strfind(current_path,'CNN_for_Classifying_Cloud_Particles');
folder_root = current_path(1:main_folder_name_position+34);
folder_path = strcat(folder_root,'\Functions');
addpath(genpath(folder_path));
clear current_path main_folder_name_position folder_path

%Load data
load(fullfile(folder_root,'\Data\2016_iHOLIMO_experiments\HOL3G'))
clear folder_root

clear data
data.nophase = get_metrics_from_matrices(HOL3G.nophase);
data.phase = get_metrics_from_matrices_folders(HOL3G.phase);
data.phase = get_medians_of_metrics(data.phase);
data.nophase = get_medians_of_metrics(data.nophase);


