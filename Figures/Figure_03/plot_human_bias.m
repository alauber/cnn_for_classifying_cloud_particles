
AnnLau = load('dataset_human_bias_AnnLau');
AnnLau = AnnLau.temp1;
FabRam = load('dataset_human_bias_FabRam');
FabRam = FabRam.temp1;
JanHen = load('dataset_human_bias_JanHen');
JanHen = JanHen.temp1;


%get particles everybody classified as droplet
clear Droplet AF AJ FJ
Droplet.Annika = find(AnnLau.class == 'Particle_round');
Droplet.Fabi = find(FabRam.class == 'Particle_round');
Droplet.Jan = find(JanHen.class == 'Particle_round');
AF = intersect(Droplet.Annika,Droplet.Fabi);
AJ = intersect(Droplet.Annika,Droplet.Jan);
FJ = intersect(Droplet.Fabi,Droplet.Jan);
Droplet.all = intersect(AF,AJ);

%get particles only two classified as droplet
AF_diff = setdiff(AF,Droplet.all);
AJ_diff = setdiff(AJ,Droplet.all);
FJ_diff = setdiff(FJ,Droplet.all);
Droplet.two = [AF_diff;AJ_diff;FJ_diff];
Droplet.two = sort(Droplet.two);

%all particles which were classified as droplets by at least two
clear min_two
min_two = [Droplet.all;Droplet.two];
min_two = sort(min_two);

%particles only classified by one as droplet
clear A F J
A = setdiff(Droplet.Annika,min_two);
F = setdiff(Droplet.Fabi,min_two);
J = setdiff(Droplet.Jan,min_two);
Droplet.one = [A;F;J];
Droplet.one = sort(Droplet.one);


%get particles everybody classified as ice
clear Ice AF AJ FJ
Ice.Annika = find(AnnLau.class == 'Particle_nubbly');
Ice.Fabi = find(FabRam.class == 'Particle_nubbly');
Ice.Jan = find(JanHen.class == 'Particle_nubbly');
AF = intersect(Ice.Annika,Ice.Fabi);
AJ = intersect(Ice.Annika,Ice.Jan);
FJ = intersect(Ice.Fabi,Ice.Jan);
Ice.all = intersect(AF,AJ);

%get particles only two classified as ice
AF_diff = setdiff(AF,Ice.all);
AJ_diff = setdiff(AJ,Ice.all);
FJ_diff = setdiff(FJ,Ice.all);
Ice.two = [AF_diff;AJ_diff;FJ_diff];
Ice.two = sort(Ice.two);

%all particles which were classified as droplets by at least two
clear min_two
min_two = [Ice.all;Ice.two];
min_two = sort(min_two);

%particles only classified by one as droplet
clear A F J
A = setdiff(Ice.Annika,min_two);
F = setdiff(Ice.Fabi,min_two);
J = setdiff(Ice.Jan,min_two);
Ice.one = [A;F;J];
Ice.one = sort(Ice.one);

%Uncertainty droplet
NDa = length(Droplet.all);
NDt = length(Droplet.two);
NDo = length(Droplet.one);
Droplet.uncertainty.one = (NDo+NDt+NDa)/(NDa+NDt)-1;
Droplet.uncertainty.all = 1-NDa/(NDa+NDt);
Droplet.mean = (NDo+NDt+2*NDa)/2;
Droplet.uncertainty.pm = 1-NDa/Droplet.mean;

%Uncertainty ice
NIa = length(Ice.all);
NIt = length(Ice.two);
NIo = length(Ice.one);
Ice.uncertainty.one = (NIo+NIt+NIa)/(NIa+NIt)-1;
Ice.uncertainty.all = 1-NIa/(NIa+NIt);
Ice.mean = (NIo+NIt+2*NIa)/2;
Ice.uncertainty.pm = 1-NIa/Ice.mean;


x_ice = [2,2,2];
x_droplet = [1,1,1];
y_ice = [NIa,Ice.mean,NIa+NIt+NIo];
y_droplet = [NDa,Droplet.mean,NDa+NDt+NDo];

%Markersize
ms = 200;

close all
figure(1)
fs = 20;
yyaxis left
scatter(1,NDa,ms,'b^','filled')%,1,Droplet.mean,'bx',1,NDa+NDt+NDo,'b^')
hold on
scatter(1,Droplet.mean,ms,'bd','filled')
hold on
scatter(1,NDa+NDt+NDo,ms,'bv','filled')
ylim([500,590])
txt = '+4%';
text(1,(Droplet.mean+NDa+NDt+NDo)/2,txt,'FontSize',fs)
txt = '-4%';
text(1,(Droplet.mean+NDa)/2,txt,'FontSize',fs)
ylabel('Number of particles labelled as liquid droplets','FontSize',fs)
yyaxis right
scatter(2,NIa,ms,'r^','filled')
hold on
scatter(2,Ice.mean,ms,'rd','filled')
hold on
scatter(2,NIa+NIt+NIo,ms,'rv','filled')

ylim([130,160])
xlim([0.5,2.5])
set(gca,'xtick',[]);
pos = [1 2];
col = {'b','r'};
names = {'Liquid droplets','Ice crystals'};
set(gca,'xtick',pos,'xticklabel',names,'FontSize',fs);
ylabel('Number of particles labelled as ice crystals','FontSize',fs)
txt = '+5%';
text(2,(Ice.mean+NIa+NIt+NIo)/2,txt,'FontSize',fs)
txt = '-5%';
text(2,(Ice.mean+NIa)/2,txt,'FontSize',fs)
