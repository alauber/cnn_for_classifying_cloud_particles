%Input: folder containing mat files of predictions done by trees or SVMs
%also with desired ending possible (e.g. \*SVM_predict.mat)
%output: struct with fields same and different for predictions on same
%datasets and predictions on different datasets
%files_path = 'C:\neural_network_cloud_particles\Data\predictions_paper\Trees_SVMs\single_to_single\*SVM_predicted.mat';
function data_new = get_data_from_predictions(files_path)


file_list = dir(files_path);
foldername = {file_list.folder};
filenames = {file_list.name};
l = length(file_list);

dataset_names = {'HOLIMO_3G','HOLIMO_3M','JFJ','2016_SON','2017_SON'};
% %Fieldnames starting with a number are not valid
dataset_names_new = {'HOL3G','HOL3M','JFJ','SON2016','SON2017'};
%find names of datasets in tree names

%counts how many same dataset predictions and predictions on different
%datasets are
s = 0;
d = 0;


for i = 1:l %Number of datasets
    % Figure out if predicted on same dataset or on different dataset
    datasets = zeros(length(dataset_names),1);
    for j = 1:length(dataset_names)
        if contains(filenames{i},dataset_names{j}) || contains(filenames{i},dataset_names_new{j}) 
            datasets(j) = 1;
        end
    end
   
    if sum(datasets) == 1
        s = s+1;
        load(fullfile(foldername{1},filenames{i}));
        data_new.same.accuracy(s,1) = data.accuracy;
        data_new.same.Droplet.accuracy(s,1) = data.Droplet.accuracy;
        try
            data_new.same.Droplet.FDR(s,1) = data.Droplet.FDR;
        catch
            data_new.same.Droplet.FDR(s,1) = 1-data.Droplet.precision;
        end
        data_new.same.Ice.accuracy(s,1) = data.Ice.accuracy;
        try
            data_new.same.Ice.FDR(s,1) = data.Ice.FDR;
        catch
            data_new.same.Ice.FDR(s,1) = 1-data.Ice.precision;
        end
        data_new.same.Artifact.accuracy(s,1) = data.Artifact.accuracy;
        try
            data_new.same.Artifact.FDR(s,1) = data.Artifact.FDR;
        catch
            data_new.same.Artifact.FDR(s,1) = 1-data.Artifact.precision;
        end
    elseif sum(datasets) == 2
        d=d+1;
        filenames_different{d} = filenames{i};
        load(fullfile(foldername{1},filenames{i}));
        data_new.different.accuracy(d,1) = data.accuracy;

        data_new.different.Droplet.accuracy(d,1) = data.Droplet.accuracy;
        try
            data_new.different.Droplet.FDR(d,1) = data.Droplet.FDR;
        catch
            data_new.different.Droplet.FDR(d,1) = 1-data.Droplet.precision;
        end
        data_new.different.Ice.accuracy(d,1) = data.Ice.accuracy;
        try
            data_new.different.Ice.FDR(d,1) = data.Ice.FDR;
        catch
            data_new.different.Ice.FDR(d,1) = 1-data.Ice.precision;
        end
        data_new.different.Artifact.accuracy(d,1) = data.Artifact.accuracy;
        try
            data_new.different.Artifact.FDR(d,1) = data.Artifact.FDR;
        catch
            data_new.different.Artifact.FDR(d,1) = 1-data.Artifact.precision;
        end
    end
end
end