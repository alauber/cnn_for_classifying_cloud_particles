%Input: struct containing fields of different datasets containing a field
%"sizeBins" which contains fields of different runs (r1, r2...) including
%fields of the different metrics (accuracy, FDR, Artifact.accuracy, etc.)
%as a vector of values for each size bin.
%Output: struct containing fields of the same datasets containing fields
%with the different metrics as a vector which contains the values of all
%three runs for the different size bins (e.g. sizebin 1 r1, size bin 1, r2,
%size bin 1 r3, size bin 2 r1 ...) and also x as the position of the markes
%for the according dataset in the different size bins.
function pred = get_plotting_data(data)
% x includes Nans if not enough data is present.

%minimum number of samples in bin
minNum = 30;

datasets = fields(data);
l = length(datasets);

%Bin sizes
bins = data.(datasets{1}).sizeBins.bins;

%Number of size bins
N = size(bins,1);

%Stepsize
s = (log(bins(size(bins,1),2))-log(bins(1,1)))/(N*l+N+1);


for i = 1:l %Number of datasets
    %Use real name of dataset
    %d = strcat('d',int2str(i)); %dataset 1,2...
    dataset = data.(datasets{i});
    run(1) = isfield(dataset.sizeBins,'r1');
    run(2) = isfield(dataset.sizeBins,'r2');
    run(3) = isfield(dataset.sizeBins,'r3');
    clear r %create a variable with includes the existing runs
    %x axis: offset stepsizs (s) times i. Different for different numbers
    %of runs
    % Create vector r with 'r1', 'r2', 'r3' depending on which run exists.
    if run(1) == 1
        r{1} = strcat('r',int2str(1));
        if run(2) == 1
            r{2} = strcat('r',int2str(2));
            if run(3) == 1
                r{3} = strcat('r',int2str(3));
            end
        elseif run(3) == 1
            r{2} = strcat('r',int2str(3));
        end
    elseif run(2) == 1
        r{1} = strcat('r',int2str(2));
        if run(3) == 1
            r{2} = strcat('r',int2str(3));
        end
    elseif run(3) == 1
        r{1} = strcat('r',int2str(3));
    end
    
    %Create values for x axis
    if ~isfield('pred.(datasets{i})','x')
        pred.(datasets{i}).x = [];
        for icnt = 1:N
            pred.(datasets{i}).x = [pred.(datasets{i}).x;repmat(exp(log(bins(icnt,1))+s*i),sum(run),1)];
        end
    end
    
    for k = 1:N %Number of size bins
        j = strcat('b',int2str(k)); %five size bins
        for rcnt = 1:sum(run) %over all existing runs
            %x values for not enough datapoints is set
            %to nan
            if dataset.sizeBins.(r{rcnt}).(j) <minNum
                pred.(datasets{i}).x(sum(run)*(k-1)+rcnt) = nan;
            end
            if rcnt == 1 && k == 1 %first value has to be treated differently
                %x values for not enough datapoints is set
                %to nan    
                metrics = fieldnames(dataset.sizeBins.(r{rcnt}));
                for mcnt = 1:length(metrics)
                    %only vectors or structs contain important dataset but
                    %have to be treated differently
                    %Look for overall values not for single classes
                    if isvector(dataset.sizeBins.(r{rcnt}).(metrics{mcnt})) && ~isstruct(dataset.sizeBins.(r{rcnt}).(metrics{mcnt})) && ~isscalar(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}))
                        if strcmp(metrics{mcnt},'accuracy') || strcmp(metrics{mcnt},'accuracyParticles')
                            %Nan values of the accuracy have to be10 since
                            %0/0 is nan but everything is correct
                            dataset.sizeBins.(r{rcnt}).(metrics{mcnt})(isnan(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}))) = 1;
                        end
                        if strcmp(metrics{mcnt},'FDR') || strcmp(metrics{mcnt},'FDRParticles') || strcmp(metrics{mcnt},'devTruthParticles')
                            %Nan values of the FDR have to be 0 since
                            %0/0 is nan but everything is correct
                            dataset.sizeBins.(r{rcnt}).(metrics{mcnt})(isnan(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}))) = 0;
                        end
                        pred.(datasets{i}).(metrics{mcnt}) = dataset.sizeBins.(r{rcnt}).(metrics{mcnt})(1);
                        %look at single class dataset (Droplets, Ice,
                        %Artifacts)       
                    elseif isstruct(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}))
                        %x values for not enough datapoints is set
                        %to nan
                        if ~isfield('pred.(datasets{i}).(metrics{mcnt})','x')
                            pred.(datasets{i}).(metrics{mcnt}).x = [];
                            for icnt = 1:N
                                pred.(datasets{i}).(metrics{mcnt}).x = [pred.(datasets{i}).(metrics{mcnt}).x;repmat(exp(log(bins(icnt,1))+s*i),sum(run),1)];
                            end
                        end
                        if dataset.sizeBins.(r{rcnt}).(metrics{mcnt}).(j) <minNum
                            pred.(datasets{i}).(metrics{mcnt}).x(sum(run)*(k-1)+rcnt) = nan;
                        end
                        metricsParticles = fieldnames(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}));
                        for mPcnt = 1:length(metricsParticles)
                            if ~isscalar(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}).(metricsParticles{mPcnt})) %b1,b2 is scalar
                                if strcmp(metricsParticles{mPcnt},'accuracy')
                                    dataset.sizeBins.(r{rcnt}).(metrics{mcnt}).(metricsParticles{mPcnt})(isnan(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}).(metricsParticles{mPcnt}))) = 1;
                                end
                                if strcmp(metricsParticles{mPcnt},'FDR') || strcmp(metricsParticles{mPcnt},'devTruth')
                                    dataset.sizeBins.(r{rcnt}).(metrics{mcnt}).(metricsParticles{mPcnt})(isnan(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}).(metricsParticles{mPcnt}))) = 0;
                                end
                                pred.(datasets{i}).(metrics{mcnt}).(metricsParticles{mPcnt}) = dataset.sizeBins.(r{rcnt}).(metrics{mcnt}).(metricsParticles{mPcnt})(1);
                            end
                            
                        end
                    end
                end %end length metrics
            else
                for mcnt = 1:length(metrics)
                    %only vectors or structs contain important dataset but
                    %have to be treated differntly
                    if isvector(dataset.sizeBins.(r{rcnt}).(metrics{mcnt})) && ~isstruct(dataset.sizeBins.(r{rcnt}).(metrics{mcnt})) && ~isscalar(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}))
                        pred.(datasets{i}).(metrics{mcnt}) = [pred.(datasets{i}).(metrics{mcnt});dataset.sizeBins.(r{rcnt}).(metrics{mcnt})(k)];
                        if strcmp(metrics{mcnt},'accuracy') || strcmp(metrics{mcnt},'accuracyParticles')
                            dataset.sizeBins.(r{rcnt}).(metrics{mcnt})(isnan(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}))) = 1;
                        end
                        if strcmp(metrics{mcnt},'FDR') || strcmp(metrics{mcnt},'FDRParticles') || strcmp(metrics{mcnt},'devTruthParticles')
                            dataset.sizeBins.(r{rcnt}).(metrics{mcnt})(isnan(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}))) = 0;
                        end
                    elseif isstruct(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}))
                        if dataset.sizeBins.(r{rcnt}).(metrics{mcnt}).(j) <minNum
                            pred.(datasets{i}).(metrics{mcnt}).x(sum(run)*(k-1)+rcnt) = nan;
                        end
                        metricsParticles = fieldnames(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}));
                        for mPcnt = 1:length(metricsParticles)
                            if ~isscalar(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}).(metricsParticles{mPcnt}))
                                if strcmp(metricsParticles{mPcnt},'accuracy')
                                    dataset.sizeBins.(r{rcnt}).(metrics{mcnt}).(metricsParticles{mPcnt})(isnan(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}).(metricsParticles{mPcnt}))) = 1;
                                end
                                if strcmp(metricsParticles{mPcnt},'FDR') || strcmp(metricsParticles{mPcnt},'devTruth')
                                    dataset.sizeBins.(r{rcnt}).(metrics{mcnt}).(metricsParticles{mPcnt})(isnan(dataset.sizeBins.(r{rcnt}).(metrics{mcnt}).(metricsParticles{mPcnt}))) = 0;
                                end
                                pred.(datasets{i}).(metrics{mcnt}).(metricsParticles{mPcnt}) = [pred.(datasets{i}).(metrics{mcnt}).(metricsParticles{mPcnt});dataset.sizeBins.(r{rcnt}).(metrics{mcnt}).(metricsParticles{mPcnt})(k)];
                            end
                        end
                    end
                end %end length metrics
            end
        end %end all runs
    end %end size bins (k) j:'b1','b2'... 
end %end datasetsets (i)
%Save bins
pred.(datasets{1}).bins = bins;
clear rcnt mcnt metrics metricsParticles mPcnt l k j i filenames filelist d s b1 b2 b3 b4 b5
end

