%Finds the confusion matrices in the experiment logfiles.
%Input: folder with txt-files or folder containing folder with txt-files
%Output: all confusion matrices in folders and subfolders as one struct
function M = get_confusion_matrices_from_txt_files(folderpath)

listing = dir(folderpath);
folders = listing([listing.isdir]);
folders(1:2) = []; %always . and ..
foldernames = {folders.name};
f = length(foldernames);
ns=0;
if f==0 %no subfolders
    f=1;
    ns = 1;
end

for j = 1:f
    if ns ==1
        filelist = dir(fullfile(folderpath,'*.txt'));
    else
        filelist = dir(fullfile(folderpath,foldernames{j},'*.txt'));
    end
    filenames = {filelist.name};
    l = length(filelist);
    
    for cnt = 1:l
        if ns ==0
            file = fullfile(folderpath,foldernames{j},filenames{cnt});
        else
            file = fullfile(folderpath,filenames{cnt});
        end
        data = textread(file, '%s');
        a = strfind(filenames{cnt},'aug_');
        name = filenames{cnt}(a:length(filenames{cnt})-4);
        s = length(data);
        a = find(strcmp(data,'matrix:'));
        data_new = data(a+1:s);
        data_num = zeros(9,1);
        z =9;
        for i = length(data_new):-1:1
            a = strfind(data_new{i},'[');
            b = strfind(data_new{i},']');
            if ~isempty(a)
                data_new{i}(a) = [];
            end
            if ~isempty(b)
                data_new{i}(b) = [];
            end
            if isempty(data_new{i})
                data_new(i) =[];
            else
                data_num(z) = str2num(data_new{i});
                z = z-1;
            end
        end
        if ns ==1
            M.(name) = vec2mat(data_num,3);
        else
            %ss: sample size (only numbers as field name not possible)
            M.(strcat('ss',foldernames{j})).(name) = vec2mat(data_num,3);
        end
    end
end
end