%Input is a data struct with fields including different evaluation metrics.
%The output is struct with the medians of the evaluation metrics of the different
%fields in one vector. 

function data_new = get_medians_of_metrics(data)
datasets = fieldnames(data);
data_new = [];
for cnt = 1:length(datasets)
    metrics = fieldnames(data.(datasets{cnt}));
    for mcnt = 1:length(metrics)
        %isstruct if Artifact Droplet or Ice
        if isstruct(data.(datasets{cnt}).(metrics{mcnt}))
            particle_metrics = fieldnames(data.(datasets{cnt}).(metrics{mcnt}));
            for pcnt = 1:length(particle_metrics)
                if ~isfield(data_new,metrics{mcnt})
                    data_new.(metrics{mcnt}) = [];
                end
                if ~isfield(data_new.(metrics{mcnt}),particle_metrics{pcnt})
                    data_new.(metrics{mcnt}).(particle_metrics{pcnt}) = [];
                end
                data_new.(metrics{mcnt}).(particle_metrics{pcnt}) = [data_new.(metrics{mcnt}).(particle_metrics{pcnt});median(data.(datasets{cnt}).(metrics{mcnt}).(particle_metrics{pcnt}))];
            end
        else
            if ~isfield(data_new,metrics{mcnt})
                data_new.(metrics{mcnt}) = [];
            end
            data_new.(metrics{mcnt}) = [data_new.(metrics{mcnt});median(data.(datasets{cnt}).(metrics{mcnt}))];
        end
    end
end
end
