%Input is a folder containing csv files and a folder containing the
%corresponding datasets
%output is a data struct with the name of the datasets as fields with all their runs and their prtclIDs and the metrics
%example: data.HOL3M.r1.Artifact.accuracy
function data = get_metrics_from_csv_folder(csv_folder,dataset_folder)
dataset_names = {'HOL3G','HOL3M','JFJ','2016_SON','2017_SON'};
%Fieldnames starting with a number are not valid
dataset_names_new = {'HOL3G','HOL3M','JFJ','SON2016','SON2017'};

datasets = dir(dataset_folder);
dataset_folder = {datasets.folder};
dataset_folder = dataset_folder{1};
datasets = {datasets.name};
datasets_new = [];
%Make sure the datasets in the folder are in the same order as the names
for i = 1:length(dataset_names)
    for j = 1:length(datasets)
        if contains(datasets{j},dataset_names{i})
            datasets_new{i} = datasets{j};
        end
    end
end
datasets = datasets_new;
clear datasets_new i
csv_files = dir(csv_folder);
csv_folder = {csv_files.folder};
csv_folder = csv_folder{1};
csv_files = {csv_files.name};

for j = 1:length(dataset_names)
    load(fullfile(dataset_folder,datasets{j})); %has to be temp
    k=1;
    r = strcat('r',int2str(k)); %names of different runs
    for i = 1:length(csv_files)
        if contains(csv_files{i},dataset_names{j})
            %%temp bisher ung�ltige Eingabe
            data.(dataset_names_new{j}).(r) = get_metrics_from_csv_file(fullfile(csv_folder,csv_files{i}),temp);
            k = k+1;
            r = strcat('r',int2str(k)); %next run
        end
    end
end


end