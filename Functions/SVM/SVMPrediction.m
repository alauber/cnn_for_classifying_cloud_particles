function data = SVMPrediction(data,SVM)

data.Artifact.class = find(data.class == 'Artifact');
data.Droplet.class = find(data.class == 'Particle_round');
data.Ice.class = find(data.class == 'Particle_nubbly');


%% Predict
[data.metricnames,~,ib] = intersect(SVM.PredictorNames,data.metricnames);
data.metricmat = data.metricmat(:,ib);
data.predicted = predict(SVM,data.metricmat);
if iscell(data.predicted)
    data.predicted = categorical(data.predicted);
end
   
data.Artifact.predicted = find(data.predicted == 'Artifact');
data.Artifact.true = intersect(data.Artifact.class,data.Artifact.predicted);
data.Artifact.false = setdiff(data.Artifact.predicted,data.Artifact.true);
data.Artifact.missing = setdiff(data.Artifact.class,data.Artifact.predicted);
data.Artifact.accuracy = length(data.Artifact.true)/length(data.Artifact.class);
data.Artifact.FDR = length(data.Artifact.false)/length(data.Artifact.predicted);
if isnan(data.Artifact.accuracy)
    data.Artifact.accuracy = 1;
end
if isnan(data.Artifact.FDR)
    data.Artifact.FDR = 0;
end

data.Droplet.predicted = find(data.predicted == 'Particle_round');
data.Droplet.true = intersect(data.Droplet.class,data.Droplet.predicted);
data.Droplet.false = setdiff(data.Droplet.predicted,data.Droplet.true);
data.Droplet.missing = setdiff(data.Droplet.class,data.Droplet.predicted);
data.Droplet.accuracy = length(data.Droplet.true)/length(data.Droplet.class);
data.Droplet.FDR = length(data.Droplet.false)/length(data.Droplet.predicted);
if isnan(data.Droplet.accuracy)
    data.Droplet.accuracy = 1;
end
if isnan(data.Droplet.FDR)
    data.Droplet.FDR = 0;
end

data.Ice.predicted = find(data.predicted == 'Particle_nubbly');
data.Ice.true = intersect(data.Ice.class,data.Ice.predicted);
data.Ice.false = setdiff(data.Ice.predicted,data.Ice.true);
data.Ice.missing = setdiff(data.Ice.class,data.Ice.predicted);
data.Ice.accuracy = length(data.Ice.true)/length(data.Ice.class);
data.Ice.FDR = length(data.Ice.false)/length(data.Ice.predicted);
if isnan(data.Ice.accuracy)
    data.Ice.accuracy = 1;
end
if isnan(data.Ice.FDR)
    data.Ice.FDR = 0;
end

data.accuracy = (length(data.Droplet.true)+length(data.Ice.true)+length(data.Artifact.true))/length(data.class);
if isnan(data.accuracy)
    data.accuracy = 1;
end

%M: Confusion Matrix
M = zeros(3,3);
M(1,1) = length(data.Artifact.true);
M(2,2) = length(data.Droplet.true);
M(3,3) = length(data.Ice.true);
M(2,1) = length(intersect(data.Artifact.class,data.Droplet.predicted));
M(3,1) = length(intersect(data.Artifact.class,data.Ice.predicted));
M(1,2) = length(intersect(data.Droplet.class,data.Artifact.predicted));
M(3,2) = length(intersect(data.Droplet.class,data.Ice.predicted));
M(1,3) = length(intersect(data.Ice.class,data.Artifact.predicted));
M(2,3) = length(intersect(data.Ice.class,data.Droplet.predicted));

end