%Add Folder Functions which is a subfolder of
%CNN_for_Classifying_Cloud_Particles folder
current_path = pwd;
main_folder_name_position = strfind(current_path,'CNN_for_Classifying_Cloud_Particles');
folder_root = current_path(1:main_folder_name_position+34);
folder_path = strcat(folder_root,'\Functions');
addpath(genpath(folder_path));
clear current_path main_folder_name_position folder_path

filelist_SVMs = dir(fullfile(folder_root,'\Data\SVMs\*single_SVM.mat'));
SVM_folder = {filelist_SVMs.folder};
SVMs = {filelist_SVMs.name};
T = length(SVMs);

for cnt = 1:T
    name_SVM{cnt} = erase(SVMs{cnt},'SVM.mat');
end

filelist_datasets = dir(fullfile(folder_root,'\Data\input_data\*.mat'));
datasets = {filelist_datasets.name};
D = length(datasets);

for cnt = 1:D
    name_dataset{cnt} = erase(datasets{cnt},'.mat');
end

for cnt = 1:D
    load(fullfile(folder_root,'\Data\input_data',datasets{cnt}));
    for i = 1:T
        if ~contains(name_SVM{i},name_dataset{cnt})
            load(fullfile(folder_root,'Data\SVMs',SVMs{i}));
            data = SVMPrediction(temp,SVM);
            name = strcat(name_SVM{i},name_dataset{cnt},'_SVM_predicted');
            data = rmfield(data,'prtclIm');
            save(name,'data')
        end
    end
end

