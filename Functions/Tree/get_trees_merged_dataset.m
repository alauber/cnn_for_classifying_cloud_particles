%Create trees on merged datasets. The name of the tree is that of the
%dataset not used for training
%kfold = 10
%penalizes by inverse class frequency of smaller class

%Add Folder Functions which is a subfolder of
%CNN_for_Classifying_Cloud_Particles folder
current_path = pwd;
main_folder_name_position = strfind(current_path,'CNN_for_Classifying_Cloud_Particles');
folder_root = current_path(1:main_folder_name_position+34);
folder_path = strcat(folder_root,'\Functions');
addpath(genpath(folder_path));
clear current_path main_folder_name_position folder_path

datasets = fullfile(folder_root,'\Data\input_data\*.mat');
dataset_list = dir(datasets);
datasets = {dataset_list.name};
clear dataset_list

kfold = 10;
data.accuracyCost = 0;
l = length(datasets);


for i = 1:l
temp = merge_datasets(fullfile(folder_root,'\Data\input_data\'),datasets{i});
%%split data
Ntrain = length(temp.class);
Nvali = round(Ntrain/kfold);
k = randperm(Ntrain);
trainVal = temp;
trainVal.metricmat = temp.metricmat(k,:);
trainVal.class = temp.class(k,:);

%training set
train = temp;
%validation set
vali = temp;

%Create kfold sets of validation set and training set
for j = 1:kfold
    if j == 1
        vali.metricmat = trainVal.metricmat(1:Nvali,:);
        vali.class =trainVal.class(1:Nvali,:);
        train.metricmat = trainVal.metricmat(Nvali+1:Ntrain,:);
        train.class = trainVal.class(Nvali+1:Ntrain,:);
    elseif j == kfold
        vali.metricmat = trainVal.metricmat((j-1)*Nvali+1:Ntrain,:);
        vali.class = trainVal.class((j-1)*Nvali+1:Ntrain,:);
        train.metricmat = trainVal.metricmat(1:(j-1)*Nvali,:);
        train.class = trainVal.class(1:(j-1)*Nvali,:);
    else
        vali.metricmat = trainVal.metricmat((j-1)*Nvali+1:j*Nvali,:);
        vali.class = trainVal.class((j-1)*Nvali+1:j*Nvali,:);
        train.metricmat = trainVal.metricmat(1:(j-1)*Nvali,:);
        train.metricmat((j-1)*Nvali+1:Ntrain-Nvali,:) = trainVal.metricmat(j*Nvali+1:Ntrain,:);
        train.class = trainVal.class(1:(j-1)*Nvali,:);
        train.class((j-1)*Nvali+1:Ntrain-Nvali,:) = trainVal.class(j*Nvali+1:Ntrain,:);
    end
    
    
    [MergedTreeFileCost] = createTree(train);
    
    %%find best pruning level
    levelCostTree = bestPruning(vali,MergedTreeFileCost);
    
    %Prediction with and without costs with best pruning level
    dataCostTree = treePrediction(vali,MergedTreeFileCost,levelCostTree);
    
    %Save best tree and best pruning level
    if dataCostTree.accuracy>data.accuracyCost
        bestCostTree = MergedTreeFileCost;
        data.bestLevelCostTree = levelCostTree;
        data.accuracyCost = dataCostTree.accuracy;
    end

end
tree = bestCostTree;
name = strsplit(datasets{i},'.');
save(strcat(name{1},'_merged_tree'),'tree')
save(strcat(name{1},'_merged_tree_data'),'data')
end

