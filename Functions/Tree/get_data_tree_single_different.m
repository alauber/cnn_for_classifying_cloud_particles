%Add Folder Functions which is a subfolder of
%CNN_for_Classifying_Cloud_Particles folder
current_path = pwd;
main_folder_name_position = strfind(current_path,'CNN_for_Classifying_Cloud_Particles');
folder_root = current_path(1:main_folder_name_position+34);
folder_path = strcat(folder_root,'\Functions');
addpath(genpath(folder_path));
clear current_path main_folder_name_position folder_path

filelist_trees = dir(fullfile(folder_root,'\Data\Trees\*single_tree.mat'));
trees_folder = {filelist_trees.folder};
trees = {filelist_trees.name};
T = length(trees);

for cnt = 1:T
    name_tree{cnt} = erase(trees{cnt},'tree.mat');
end

filelist_datasets = dir(fullfile(folder_root,'\Data\input_data\*.mat'));
datasets = {filelist_datasets.name};
D = length(datasets);

for cnt = 1:D
    name_dataset{cnt} = erase(datasets{cnt},'.mat');
end

for cnt = 1:D
    load(fullfile(folder_root,'\Data\input_data',datasets{cnt}));
    for i = 1:T
        if ~contains(name_tree{i},name_dataset{cnt})
            load(fullfile(trees_folder{1},trees{i}));
            data = treePrediction(temp,tree,0);
            name = strcat(name_tree{i},name_dataset{cnt},'_tree_predicted');
            data = rmfield(data,'prtclIm');
            save(name,'data')
        end
    end
end

