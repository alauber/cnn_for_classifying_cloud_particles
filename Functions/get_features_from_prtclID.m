%Input: struct with field prtclID
%Output: same struct with additional field of the x, y and z position
function data = get_features_from_prtclID(data)
%data.rx has to include the field prtclID
if isfield(data,'prtclID')
    data = getSize(data);
else
    datasets = fieldnames(data);
    for cnt = 1:length(datasets)
        if isfield(data.(datasets{cnt}),'prtclID')
            data.(datasets{cnt}) = get_features(data.(datasets{cnt}));
        else
            runs = fieldnames(data.(datasets{cnt}));
            for i = 1:length(runs)
                if isfield(data.(datasets{cnt}).(runs{i}),'prtclID')
                    data.(datasets{cnt}).(runs{i}) = get_features(data.(datasets{cnt}).(runs{i}));
                end
            end
        end
    end
end