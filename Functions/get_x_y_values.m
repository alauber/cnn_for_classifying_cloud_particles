%metric: e.g. 'accuracy', 'FDR'
%particle: 'Droplet', 'Ice', 'Artifact' or empty if the overall accuracy
%   or FDR is wanted
%data: has fields with evaluation matrices or fields including the number
%   of the sample sizes
%datasets: shortcut of name of dataset of interest
%datasets_new: shortcut of new name which is wanted to be given to dataset
%   of interest

%Output: field names "datasets_new" including the fields x and y. x is the
%   number of samples used and y the value of the chosen evaluation metric

function y = get_x_y_values(metric,particle,data,datasets,datasets_names)

for i = 1:length(datasets)
    y.(datasets_names{i}).y = {};
end
clear i

if ~isfield(data,'accuracy')
    %get sample sizes and sort according to the size
    ss = fieldnames(data);
    for i = 1:length(ss)
        %%bugfix necessary
        x= strsplit(ss{i},'ss');
        numbers{i,2} = x{2};
        ss_new(i) = str2num(numbers{i,2});
    end
    clear numbers x i
    [ss_new,ind_FT] = sort(ss_new);
    
    ss = ss(ind_FT);
    for i = 1:length(ss) %different sample sizes
        augs = fieldnames(data.(ss{i}));
        for j = 1:length(datasets) %different experiments
            for k = 1:length(augs) %Find fields for specific datasets
                if contains(augs{k},datasets{j})
                    if ~isempty(particle)
                        if isempty(y.(datasets_names{j}).y)
                            y.(datasets_names{j}).y = data.(ss{i}).(augs{k}).(particle).(metric);
                            y.(datasets_names{j}).x = repmat(ss_new(i),length(y.(datasets_names{j}).y),1);
                        else
                            y.(datasets_names{j}).y = [y.(datasets_names{j}).y;data.(ss{i}).(augs{k}).(particle).(metric)];
                            y.(datasets_names{j}).x = [y.(datasets_names{j}).x;repmat(ss_new(i),length(data.(ss{i}).(augs{k}).(particle).(metric)),1)];
                        end
                    else
                        if isempty(y.(datasets_names{j}).y)
                            y.(datasets_names{j}).y = data.(ss{i}).(augs{k}).(metric);
                            y.(datasets_names{j}).x = repmat(ss_new(i),length(y.(datasets_names{j}).y),1);
                        else
                            y.(datasets_names{j}).y = [y.(datasets_names{j}).y;data.(ss{i}).(augs{k}).(metric)];
                            y.(datasets_names{j}).x = [y.(datasets_names{j}).x;repmat(ss_new(i),length(data.(ss{i}).(augs{k}).(metric)),1)];
                        end
                    end
                    break
                end
            end
        end
    end
else
    augs = fieldnames(data);
    for j = 1:length(datasets) %different experiments
        for k = 1:length(augs) %Find fields for specific datasets
            if contains(augs{k},datasets{j})
                if ~isempty(particle)
                    y.(datasets_names{j}).y = data.(augs{k}).(particle).(metric);
                else
                    y.(datasets_names{j}).y = data.(augs{k}).(metric);
                end
                break
            end
        end
    end
end

end